# Bitbucket REST API Clients

Type safe REST clients for [Bitbucket Cloud](cloud) and
[Bitbucket Server](server). Designed initially for the needs
of [Bitbucket Linky](https://bitbucket.org/atlassianlabs/intellij-bitbucket-references-plugin)
plugin for IntelliJ IDEs, they are maintained as a standalone library.

## Development

To build the library:

* run `mvn package`

## License and Copyright

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
