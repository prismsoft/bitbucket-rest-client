# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [4.0.3](http://bitbucket.org/dpenkin/bitbucket-rest-client/compare/v4.0.3..v4.0.2) (2019-08-20)


### 🚑 Bug Fixes

* **OAuth:** Rework OAuth refresh token procedure in a more efficient way ([71103b2](http://bitbucket.org/dpenkin/bitbucket-rest-client/commits/71103b2))



### [4.0.2](http://bitbucket.org/dpenkin/bitbucket-rest-client/compare/v4.0.2..v4.0.1) (2019-07-26)


### 🚑 Bug Fixes

* Fix basic auth for refresh OAuth token calls ([cfc5a1f](http://bitbucket.org/dpenkin/bitbucket-rest-client/commits/cfc5a1f))



### [4.0.1](http://bitbucket.org/dpenkin/bitbucket-rest-client/compare/v4.0.1..v4.0.0) (2019-07-10)


### 🚑 Bug Fixes

* Handle 400 (Bad Request) response from PR Links ([d868315](http://bitbucket.org/dpenkin/bitbucket-rest-client/commits/d868315))



## [4.0.0](http://bitbucket.org/dpenkin/bitbucket-rest-client/compare/v4.0.0..v3.0.0) (2019-07-09)


### 🚀 Features

* Update to Ktor 1.2 ([3424b0d](http://bitbucket.org/dpenkin/bitbucket-rest-client/commits/3424b0d))


### BREAKING CHANGES

* Snippet creation API now requires provider of an InputStream for the file content



# [3.0.0](http://bitbucket.org/dpenkin/bitbucket-rest-client/compare/v3.0.0..v3.0.0-b6) (2019-06-06)
