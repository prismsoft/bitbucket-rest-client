package com.atlassian.bitbucket.server.rest.personalaccesstoken

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.common.mapErrors
import com.atlassian.bitbucket.server.rest.pagination.PagedBean
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pagination.ServerPaginationApi
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.put
import io.ktor.http.*
import java.net.URI

internal const val PROJECT_PERMISSION_PREFIX = "PROJECT"
internal const val REPOSITORY_PERMISSION_PREFIX = "REPO"

private val JSON_CONTENT_TYPE = ContentType.parse("application/json")

class PersonalAccessTokensApiImpl(private val bitbucket: BitbucketServer,
                                  private val client: HttpClient,
                                  private val username: String) : PersonalAccessTokensApi {

    override suspend fun create(name: String, permissions: Iterable<Permission>): PersonalAccessToken {
        val url = bitbucket.personalAccessTokensUrl(username)
        return mapErrors("create personal access token") {
            client.put<PersonalAccessTokenBean>(url) {
                body = CreatePersonalAccessTokenBean(name, permissions.map { it.getCode() })
                contentType(JSON_CONTENT_TYPE)
            }.toPersonalAccessToken()
        }
    }

    override suspend fun page(): ServerPage<PersonalAccessTokenInfo> {
        val url = bitbucket.personalAccessTokensUrl(username)
        return mapErrors("personal access tokens") {
            client.get<PagedBean<PersonalAccessTokenInfoBean>>(url)
                    .toPersonalAccessTokensPage(url)
        }
    }
}

internal class PersonalAccessTokenInfosPagination(private val client: HttpClient,
                                                  private val pageUrl: URI) : ServerPaginationApi<PersonalAccessTokenInfo> {

    override suspend fun nextPage(): ServerPage<PersonalAccessTokenInfo> =
            mapErrors("personal access tokens") {
                client.get<PagedBean<PersonalAccessTokenInfoBean>>(pageUrl.toURL())
                        .toPersonalAccessTokensPage(URLBuilder().apply { takeFrom(pageUrl) }.build())
            }
}

internal fun PagedBean<PersonalAccessTokenInfoBean>.toPersonalAccessTokensPage(url: Url) =
        toPage(url, { it.toPersonalAccessTokenInfo() }) { it::paginatePersonalAccessTokenInfos }
