package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.common.mapErrors
import com.atlassian.bitbucket.server.rest.pagination.PagedBean
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pagination.ServerPaginationApi
import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import java.net.URI

internal class PullRequestsApiImpl(private val bitbucket: BitbucketServer,
                                   private val client: HttpClient,
                                   private val projectKey: String,
                                   private val repositorySlug: String) : PullRequestsApi {
    // TODO convert to builder here and in Cloud
    private var authorUsername: String? = null
    private var reviewerUsernames: List<String> = listOf()
    private var state: PullRequestState? = null
    private var targetBranchName: String? = null

    override fun withAuthor(username: String): PullRequestsApi {
        this.authorUsername = username
        return this
    }

    override fun withReviewer(username: String): PullRequestsApi {
        this.reviewerUsernames += username
        return this
    }

    override fun withState(state: PullRequestState): PullRequestsApi {
        this.state = state
        return this
    }

    override fun withTargetBranch(branchName: String): PullRequestsApi {
        this.targetBranchName = branchName
        return this
    }

    override suspend fun page(): ServerPage<PullRequest> {
        val url = bitbucket.pullRequestsUrl(
                projectKey,
                repositorySlug,
                state,
                targetBranchName,
                authorUsername,
                reviewerUsernames
        )
        return mapErrors("pull requests") {
            client.get<PagedBean<PullRequestBean>>(url)
                    .toPullRequestsPage(url)
        }
    }
}

internal class PullRequestsPagination(private val client: HttpClient,
                                      private val pageUrl: URI) : ServerPaginationApi<PullRequest> {

    override suspend fun nextPage(): ServerPage<PullRequest> =
            mapErrors("pull requests") {
                client.get<PagedBean<PullRequestBean>>(pageUrl.toURL())
                        .toPullRequestsPage(URLBuilder().apply { takeFrom(pageUrl) }.build())
            }
}

internal class PullRequestApiImpl(private val bitbucket: BitbucketServer,
                                  private val client: HttpClient,
                                  private val projectKey: String,
                                  private val repositorySlug: String,
                                  private val id: Long) : PullRequestApi {
    override fun comment(commentId: Long): PullRequestCommentApi =
            PullRequestCommentApiImpl(bitbucket, client, projectKey, repositorySlug, id, commentId)

    override fun comments(): PullRequestCommentsApi =
            PullRequestCommentsApiImpl(bitbucket, client, projectKey, repositorySlug, id)


    override suspend fun get(): PullRequest =
            mapErrors("pull request by ID") {
                client.get<PullRequestBean>(bitbucket.pullRequestUrl(projectKey, repositorySlug, id))
                        .toPullRequest()
            }

    override suspend fun approve() =
            mapErrors("approve pull request") {
                client.post<Unit>(bitbucket.approvePullRequestUrl(projectKey, repositorySlug, id))
            }

    override suspend fun unapprove() =
            mapErrors("unapprove pull request") {
                client.delete<Unit>(bitbucket.approvePullRequestUrl(projectKey, repositorySlug, id))
            }
}

internal fun PagedBean<PullRequestBean>.toPullRequestsPage(url: Url) =
        toPage(url, { it.toPullRequest() }) { it::paginatePullRequests }
