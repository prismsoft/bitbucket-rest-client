package com.atlassian.bitbucket.server.rest.config

import com.atlassian.bitbucket.server.rest.MalformedResponse
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import io.ktor.client.HttpClientConfig
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import java.time.Instant

fun HttpClientConfig<*>.configureSerialization() {
    install(JsonFeature) {
        serializer = JacksonSerializer {
            enable(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES)
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

            registerModule(SimpleModule()
                    .addDeserializer(Instant::class.java, object : JsonDeserializer<Instant>() {
                        override fun deserialize(p: JsonParser, ctx: DeserializationContext) =
                                p.text?.toLong()?.let { Instant.ofEpochMilli(it) }
                                        ?: throw MalformedResponse("Failed to parse timestamp from '${p.currentName}: ${p.text}'")
                    })
                    .addSerializer(Instant::class.java, object : JsonSerializer<Instant>() {
                        override fun serialize(value: Instant, json: JsonGenerator, serializers: SerializerProvider) =
                                json.writeString(value.toEpochMilli().toString())
                    }))
        }
    }
}
