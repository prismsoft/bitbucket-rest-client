package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.common.LinksBean
import com.atlassian.bitbucket.server.rest.common.toLinks
import com.atlassian.bitbucket.server.rest.user.UserBean
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import java.time.Instant

internal fun BitbucketServer.pullRequestsUrl(projectKey: String,
                                             repoSlug: String,
                                             state: PullRequestState? = null,
                                             atRefName: String? = null,
                                             authorUsername: String? = null,
                                             reviewerUsernames: List<String> = listOf()): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests"))
            state?.let { parameters.append("state", it.name) }
            atRefName?.let { parameters.append("at", it) }

            var index = 0
            val addUser = { username: String, role: PullRequestUserRole ->
                parameters.append("username.${++index}", username)
                parameters.append("role.$index", role.name)
            }

            authorUsername?.let { addUser(it, PullRequestUserRole.AUTHOR) }
            reviewerUsernames.forEach { addUser(it, PullRequestUserRole.REVIEWER) }
        }.build()

internal fun BitbucketServer.pullRequestUrl(projectKey: String,
                                            repoSlug: String,
                                            id: Long): Url =
        apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests/$id")

internal fun BitbucketServer.approvePullRequestUrl(projectKey: String,
                                                   repoSlug: String,
                                                   id: Long): Url =
        apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests/$id/approve")

internal data class PullRequestBean(val id: Long,
                                    val title: String,
                                    val description: String?,
                                    val state: PullRequestState,
                                    val createdDate: Instant,
                                    val updatedDate: Instant,
                                    val toRef: RefInfoBean,
                                    val fromRef: RefInfoBean,
                                    val author: AuthorBean,
                                    val reviewers: List<ReviewerBean> = listOf(),
                                    val links: LinksBean) {
    fun toPullRequest() = PullRequest(
            id = id,
            title = title,
            description = description,
            state = state,
            createdDate = createdDate,
            updatedDate = updatedDate,
            destination = toRef.toRefInfo(),
            source = fromRef.toRefInfo(),
            author = author.user.toUser(),
            reviewers = reviewers.map { it.user.toUser() },
            links = links.toLinks()
    )
}

internal data class AuthorBean(val user: UserBean)
internal data class ReviewerBean(val user: UserBean)

internal data class RefInfoBean(val id: String,
                                val displayId: String,
                                val latestCommit: String,
                                val repository: RepositoryInfoBean) {
    fun toRefInfo() = RefInfo(id, displayId, latestCommit, repository.toRepositoryInfo())
}

internal data class RepositoryInfoBean(val name: String,
                                       val slug: String,
                                       val project: ProjectInfoBean) {
    fun toRepositoryInfo() = RepositoryInfo(name, slug, project.key, project.name)
}

internal data class ProjectInfoBean(val key: String,
                                    val name: String)

internal enum class PullRequestUserRole {
    AUTHOR, REVIEWER, PARTICIPANT
}
