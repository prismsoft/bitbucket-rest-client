package com.atlassian.bitbucket.server.rest.commit

import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequest

interface CommitApi {
    suspend fun pagePullRequests(): ServerPage<PullRequest>
}
