package com.atlassian.bitbucket.server.rest.repository

import com.atlassian.bitbucket.server.rest.CommitHash
import com.atlassian.bitbucket.server.rest.commit.CommitApi
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestApi
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestsApi

interface RepositoryApi {
    fun commit(commitHash: CommitHash): CommitApi

    fun pullRequest(pullRequestId: Long): PullRequestApi
    fun pullRequests(): PullRequestsApi
}
