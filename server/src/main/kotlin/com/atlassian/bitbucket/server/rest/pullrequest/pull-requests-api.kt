package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.CommitHash
import com.atlassian.bitbucket.server.rest.common.Links
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.user.User
import java.time.Instant

interface PullRequestApi {
    fun comment(commentId: Long): PullRequestCommentApi
    fun comments(): PullRequestCommentsApi

    suspend fun get(): PullRequest
    suspend fun approve()
    suspend fun unapprove()
}

interface PullRequestsApi {
    // TODO get rid of builder API, make room for create()
    fun withAuthor(username: String): PullRequestsApi

    fun withReviewer(username: String): PullRequestsApi
    fun withState(state: PullRequestState): PullRequestsApi
    fun withTargetBranch(branchName: String): PullRequestsApi

    suspend fun page(): ServerPage<PullRequest>
}

data class PullRequest(val id: Long,
                       val title: String,
                       val description: String?,
                       val state: PullRequestState,
                       val createdDate: Instant,
                       val updatedDate: Instant,
                       val destination: RefInfo,
                       val source: RefInfo,
                       val author: User,
                       val reviewers: List<User>,
                       val links: Links)

data class RefInfo(val id: String,
                   val name: String,
                   val commitHash: CommitHash,
                   val repositoryInfo: RepositoryInfo)

data class RepositoryInfo(val name: String,
                          val slug: String,
                          val projectKey: String,
                          val projectName: String)

enum class PullRequestState {
    MERGED, OPEN, DECLINED
}
