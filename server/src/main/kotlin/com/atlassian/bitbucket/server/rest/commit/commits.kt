package com.atlassian.bitbucket.server.rest.commit

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.CommitHash
import com.atlassian.bitbucket.server.rest.common.mapErrors
import com.atlassian.bitbucket.server.rest.pagination.PagedBean
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestBean
import com.atlassian.bitbucket.server.rest.pullrequest.toPullRequestsPage
import io.ktor.client.HttpClient
import io.ktor.client.request.get

internal class CommitApiImpl(private val bitbucket: BitbucketServer,
                             private val client: HttpClient,
                             private val projectKey: String,
                             private val repositorySlug: String,
                             private val commitHash: CommitHash) : CommitApi {

    override suspend fun pagePullRequests(): ServerPage<PullRequest> =
            mapErrors("commit pull requests") {
                val url = bitbucket.relatedPullRequestsUrl(projectKey, repositorySlug, commitHash)
                client.get<PagedBean<PullRequestBean>>(url)
                        .toPullRequestsPage(url)
            }
}
