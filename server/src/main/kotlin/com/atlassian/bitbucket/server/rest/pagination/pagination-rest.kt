package com.atlassian.bitbucket.server.rest.pagination

import com.atlassian.bitbucket.server.rest.BitbucketServerApiImpl
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import io.ktor.http.toURI
import java.net.URI

internal data class PagedBean<T>(val start: Int,
                                 val isLastPage: Boolean,
                                 val limit: Int,
                                 val nextPageStart: Int? = null,
                                 val values: List<T> = emptyList()) {
    fun <R> toPage(url: Url,
                   valueConverter: (T) -> R,
                   paginator: (BitbucketServerApiImpl) -> (URI?) -> ServerPaginationApi<R>?): ServerPage<R> {
        val nextPageUrl = if (isLastPage) null else URLBuilder().apply {
            takeFrom(url)
            parameters["start"] = "$nextPageStart"
        }.build().toURI()

        return ServerPage(
                pageLength = limit,
                items = values.map { valueConverter(it) },
                nextPageUrl = nextPageUrl,
                paginator = { api -> paginator(api)(nextPageUrl) }
        )
    }
}
