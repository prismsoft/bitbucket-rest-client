package com.atlassian.bitbucket.server.rest.common


import java.net.URI

internal typealias LinksBean = Map<String, List<LinkBean>>

internal fun LinksBean.toLinks(): Links =
        flatMap { (key, value) ->
            value.map { link ->
                val suffix = link.name?.let { ".$it" } ?: ""
                "$key$suffix" to link.toLink()
            }
        }.toMap()

internal data class LinkBean(val name: String?, val href: String) {
    fun toLink(): Link = Link(URI.create(href))
}
