package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.common.Links
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.user.User
import java.time.Instant

interface PullRequestCommentApi {
    suspend fun get(): Comment
}

interface PullRequestCommentsApi {
    fun withFile(path: String): PullRequestCommentsApi

    suspend fun page(): ServerPage<Comment>
}

data class Comment(val id: Long,
                   val content: CommentContent,
                   val createdDate: Instant,
                   val updatedDate: Instant,
                   val author: User,
                   val anchor: CommentAnchor?,
                   val replies: List<Comment>,
                   val editable: Boolean,
                   val deletable: Boolean,
                   val links: Links)

data class CommentContent(val html: String,
                          val raw: String)

data class CommentAnchor(val filePath: String,
                         val sourceLineNumber: Int?,
                         val destinationLineNumber: Int?,
                         val orphaned: Boolean)

