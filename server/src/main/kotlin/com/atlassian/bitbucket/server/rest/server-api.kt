package com.atlassian.bitbucket.server.rest

import com.atlassian.bitbucket.HttpClientLifecyclePolicy
import com.atlassian.bitbucket.server.rest.Authentication.Guest
import com.atlassian.bitbucket.server.rest.connectivity.ConnectivityTestApi
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.personalaccesstoken.PersonalAccessTokensApi
import com.atlassian.bitbucket.server.rest.repository.RepositoryApi
import io.ktor.client.HttpClient
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import kotlinx.io.core.Closeable
import java.net.URI

typealias CommitHash = String

interface BitbucketServerApi : Closeable {
    fun repository(projectKey: String, repositorySlug: String, isUser: Boolean = false): RepositoryApi
    fun personalAccessTokens(username: String): PersonalAccessTokensApi

    suspend fun <T> nextPage(page: ServerPage<T>): ServerPage<T>?

    fun testConnectivity(): ConnectivityTestApi

    companion object {
        operator fun invoke(config: BitbucketServerApiConfig.() -> Unit = { }): BitbucketServerApi =
                BitbucketServerApiImpl(config)
    }
}

class BitbucketServerApiConfig {
    internal var httpClientBuilder: () -> HttpClient = { HttpClient() }
    internal var httpClientLifecyclePolicy: HttpClientLifecyclePolicy = HttpClientLifecyclePolicy.BOUND
    internal var onCloseAction: () -> Unit = {}

    /**
     * Instance of Bitbucket Server to connect to.
     */
    lateinit var instance: BitbucketServer

    /**
     * Authentication to use when requesting Bitbucket Server.
     */
    var authentication: Authentication = Guest

    /**
     * Allows to customize the configuration of the underlying HTTP client.
     *
     * @param lifecyclePolicy lifecyle policy for the underlying HTTP client
     * @param builder function that creates an [HttpClient] instance
     */
    fun httpClient(lifecyclePolicy: HttpClientLifecyclePolicy = HttpClientLifecyclePolicy.BOUND,
                   builder: () -> HttpClient) {
        httpClientBuilder = builder
        httpClientLifecyclePolicy = lifecyclePolicy
    }

    /**
     * Custom action to run when REST API client is [kotlinx.io.core.CloseableKt][closed].
     *
     * @param block code to execute
     */
    fun onClose(block: () -> Unit) {
        onCloseAction = block
    }
}

// TODO normalize URL
data class BitbucketServer(val baseUrl: Url) {
    constructor(baseUrl: URI) : this(URLBuilder().apply { takeFrom(baseUrl) }.build())
    constructor(baseUrl: String) : this(URLBuilder().apply { takeFrom(baseUrl) }.build())

    val apiBaseUrl = baseUrl
}

sealed class Authentication {
    object Guest : Authentication()
    data class Basic(val username: String, val password: String) : Authentication()
    data class Bearer(val token: String) : Authentication()
}

open class BitbucketServerRestClientException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

open class BadResponseCodeException(status: HttpStatusCode, val endpointName: String, val url: Url)
    : BitbucketServerRestClientException("Bitbucket responded with ${status.value} (${status.description}) for $endpointName endpoint [$url]") {
    val code = status.value
    val description = status.description
}

class ForbiddenException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class UnauthorizedException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class NotFoundException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class MalformedResponse(message: String, cause: Throwable? = null) : BitbucketServerRestClientException(message, cause)
