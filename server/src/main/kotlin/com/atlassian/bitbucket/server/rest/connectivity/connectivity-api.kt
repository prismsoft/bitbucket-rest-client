package com.atlassian.bitbucket.server.rest.connectivity

interface ConnectivityTestApi {

    suspend fun authenticatedResource()

    suspend fun unauthenticatedResource()
}
