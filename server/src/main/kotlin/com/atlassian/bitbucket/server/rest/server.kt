package com.atlassian.bitbucket.server.rest

import com.atlassian.bitbucket.server.rest.config.configureSerialization
import com.atlassian.bitbucket.server.rest.connectivity.ConnectivityTestApi
import com.atlassian.bitbucket.server.rest.connectivity.ConnectivityTestApiImpl
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pagination.ServerPaginationApi
import com.atlassian.bitbucket.server.rest.personalaccesstoken.PersonalAccessTokenInfo
import com.atlassian.bitbucket.server.rest.personalaccesstoken.PersonalAccessTokenInfosPagination
import com.atlassian.bitbucket.server.rest.personalaccesstoken.PersonalAccessTokensApi
import com.atlassian.bitbucket.server.rest.personalaccesstoken.PersonalAccessTokensApiImpl
import com.atlassian.bitbucket.server.rest.pullrequest.Comment
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestCommentsPagination
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestsPagination
import com.atlassian.bitbucket.server.rest.repository.RepositoryApi
import com.atlassian.bitbucket.server.rest.repository.RepositoryApiImpl
import com.atlassian.bitbucket.util.closeIfRequired
import com.atlassian.bitbucket.util.pagination.PaginationApiProvider
import io.ktor.client.HttpClient
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.features.defaultRequest
import io.ktor.client.request.header
import java.net.URI

internal class BitbucketServerApiImpl(customConfig: BitbucketServerApiConfig.() -> Unit) : BitbucketServerApi, PaginationApiProvider {

    private val config = BitbucketServerApiConfig().apply(customConfig)
    private val httpClient: HttpClient

    init {
        with(config) {
            val client = httpClientBuilder()
            val auth = authentication
            httpClient = when (auth) {
                is Authentication.Basic -> client.config {
                    install(Auth) {
                        basic {
                            username = auth.username
                            password = auth.password
                        }
                    }
                }
                is Authentication.Bearer -> client.config {
                    defaultRequest {
                        header("Authorization", "Bearer ${auth.token}")
                    }
                }
                is Authentication.Guest -> client
            }.config {
                configureSerialization()
            }
        }
    }

    override fun close() = with(config) {
        onCloseAction()
        httpClient.closeIfRequired(httpClientLifecyclePolicy)
    }

    override fun repository(projectKey: String, repositorySlug: String, isUser: Boolean): RepositoryApi =
            RepositoryApiImpl(config.instance, httpClient, projectKey, repositorySlug)

    override fun personalAccessTokens(username: String): PersonalAccessTokensApi =
            PersonalAccessTokensApiImpl(config.instance, httpClient, username)

    override fun testConnectivity(): ConnectivityTestApi = ConnectivityTestApiImpl(config.instance, httpClient)

    override suspend fun <T> nextPage(page: ServerPage<T>): ServerPage<T>? =
            page.paginator(this)?.nextPage()

    internal fun paginatePullRequests(nextPageUrl: URI?): ServerPaginationApi<PullRequest>? =
            nextPageUrl?.let { PullRequestsPagination(httpClient, it) }

    internal fun paginateComments(nextPageUrl: URI?): ServerPaginationApi<Comment>? =
            nextPageUrl?.let { PullRequestCommentsPagination(httpClient, it) }

    internal fun paginatePersonalAccessTokenInfos(nextPageUrl: URI?): ServerPaginationApi<PersonalAccessTokenInfo>? =
            nextPageUrl?.let { PersonalAccessTokenInfosPagination(httpClient, it) }
}
