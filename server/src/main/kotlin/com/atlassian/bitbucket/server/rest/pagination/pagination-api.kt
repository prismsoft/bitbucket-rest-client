package com.atlassian.bitbucket.server.rest.pagination

import com.atlassian.bitbucket.server.rest.BitbucketServerApiImpl
import com.atlassian.bitbucket.util.pagination.Page
import com.atlassian.bitbucket.util.pagination.PaginationApi
import java.net.URI

internal typealias ServerPaginationApi<T> = PaginationApi<ServerPage<T>, BitbucketServerApiImpl, T>

data class ServerPage<T> internal constructor(override val pageLength: Int,
                                              override val items: List<T>,
                                              override val nextPageUrl: URI?,
                                              override val paginator: (BitbucketServerApiImpl) -> ServerPaginationApi<T>?
) : Page<T, BitbucketServerApiImpl>

