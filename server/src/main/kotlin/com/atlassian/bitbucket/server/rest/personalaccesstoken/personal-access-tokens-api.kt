package com.atlassian.bitbucket.server.rest.personalaccesstoken

import com.atlassian.bitbucket.server.rest.pagination.ServerPage

interface PersonalAccessTokensApi {
    suspend fun create(name: String, permissions: Iterable<Permission>): PersonalAccessToken

    suspend fun page(): ServerPage<PersonalAccessTokenInfo>
}

enum class AccessLevel {
    READ, WRITE, ADMIN
}

sealed class Permission(internal open val entity: String, open val accessLevel: AccessLevel) {

    fun getCode() = "${entity}_${accessLevel.name}"

    data class ProjectPermission(override val accessLevel: AccessLevel) : Permission("PROJECT", accessLevel)
    data class RepositoryPermission(override val accessLevel: AccessLevel) : Permission("REPO", accessLevel)
    data class UnknownEntityPermission(override val entity: String,
                                       override val accessLevel: AccessLevel) : Permission(entity, accessLevel)
}

data class PersonalAccessTokenInfo(val id: String,
                                   val name: String,
                                   val permissions: List<Permission>)

data class PersonalAccessToken(val id: String,
                               val name: String,
                               val token: String,
                               val permissions: List<Permission>)
