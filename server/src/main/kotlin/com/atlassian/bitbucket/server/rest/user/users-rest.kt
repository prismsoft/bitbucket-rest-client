package com.atlassian.bitbucket.server.rest.user

import com.atlassian.bitbucket.server.rest.common.LinksBean
import com.atlassian.bitbucket.server.rest.common.toLinks
import com.fasterxml.jackson.annotation.JsonProperty

internal data class UserBean(@JsonProperty("displayName") val name: String,
                             @JsonProperty("slug") val username: String,
                             val links: LinksBean) {
    fun toUser() = User(name, username, links.toLinks())
}
