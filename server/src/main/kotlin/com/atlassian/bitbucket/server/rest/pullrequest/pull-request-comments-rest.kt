package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.common.LinksBean
import com.atlassian.bitbucket.server.rest.common.toLinks
import com.atlassian.bitbucket.server.rest.user.UserBean
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import java.net.URI
import java.time.Instant

internal fun BitbucketServer.pullRequestActivitiesUrl(projectKey: String,
                                                      repoSlug: String,
                                                      id: Long,
                                                      start: Int? = null): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests/$id/activities"))
            parameters.append("markup", true.toString())
            start?.let { parameters.append("start", it.toString()) }
        }.build()

internal fun BitbucketServer.pullRequestFileCommentsUrl(projectKey: String,
                                                        repoSlug: String,
                                                        id: Long,
                                                        filePath: String): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests/$id/comments"))
            parameters.append("markup", true.toString())
            parameters.append("path", filePath)
        }.build()

// Does not return comment anchor
internal fun BitbucketServer.pullRequestCommentUrl(projectKey: String,
                                                   repoSlug: String,
                                                   pullRequestId: Long,
                                                   id: Long): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/pull-requests/$pullRequestId/comments/$id"))
            parameters.append("markup", true.toString())
        }.build()

internal fun URI.matchesPullRequestActivitiesUrl() = path.contains("/pull-requests/") && path.endsWith("/activities")

internal data class ActivityBean(val action: PullRequestAction,
                                 val createdDate: Instant,
                                 val user: UserBean,
                                 val commentAction: PullRequestCommentAction?,
                                 val comment: CommentBean?,
                                 val commentAnchor: CommentAnchorBean?) {
    fun toComment() = comment?.toComment(commentAnchor) ?: throw IllegalStateException("Not a comment activity")
}

internal data class CommentBean(val id: Long,
                                val text: String,
                                val html: String,
                                val author: UserBean,
                                val createdDate: Instant,
                                val updatedDate: Instant,
                                val comments: List<CommentBean>,
                                val anchor: CommentAnchorBean?,
                                val permittedOperations: CommentOperationsBean,
                                val links: LinksBean) {
    fun toComment(extAnchor: CommentAnchorBean? = null): Comment =
            Comment(
                    id = id,
                    content = CommentContent(html, text),
                    createdDate = createdDate,
                    updatedDate = updatedDate,
                    author = author.toUser(),
                    anchor = (extAnchor ?: anchor)?.toCommentAnchor(),
                    replies = comments.map { it.toComment() },
                    editable = permittedOperations.editable,
                    deletable = permittedOperations.deletable,
                    links = links.toLinks()
            )
}

internal data class CommentAnchorBean(val line: Int,
                                      val lineType: CommentAnchorLineType,
                                      val fileType: CommentAnchorFileType,
                                      val path: String,
                                      val orphaned: Boolean) {
    fun toCommentAnchor() = when (fileType) {
        CommentAnchorFileType.FROM -> CommentAnchor(path, line, null, orphaned)
        CommentAnchorFileType.TO -> CommentAnchor(path, null, line, orphaned)
    }
}

internal data class CommentOperationsBean(val editable: Boolean,
                                          val deletable: Boolean)

internal enum class CommentAnchorFileType {
    FROM, TO
}

internal enum class CommentAnchorLineType {
    ADDED, REMOVED, CONTEXT
}

internal enum class PullRequestAction {
    DECLINED, COMMENTED, MERGED, OPENED, REOPENED, RESCOPED, UPDATED, APPROVED, UNAPPROVED, REVIEWED
}

internal enum class PullRequestCommentAction {
    ADDED, DELETED, EDITED, REPLIED
}

internal enum class PullRequestActivityType {
    ACTIVITY, COMMENT
}
