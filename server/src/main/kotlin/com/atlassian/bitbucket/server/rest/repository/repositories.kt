package com.atlassian.bitbucket.server.rest.repository

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.CommitHash
import com.atlassian.bitbucket.server.rest.commit.CommitApi
import com.atlassian.bitbucket.server.rest.commit.CommitApiImpl
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestApi
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestApiImpl
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestsApi
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestsApiImpl
import io.ktor.client.HttpClient

internal class RepositoryApiImpl(private val bitbucket: BitbucketServer,
                                 private val client: HttpClient,
                                 private val projectKey: String,
                                 private val repositorySlug: String) : RepositoryApi {
    override fun commit(commitHash: CommitHash): CommitApi =
            CommitApiImpl(bitbucket, client, projectKey, repositorySlug, commitHash)

    override fun pullRequest(pullRequestId: Long): PullRequestApi =
            PullRequestApiImpl(bitbucket, client, projectKey, repositorySlug, pullRequestId)

    override fun pullRequests(): PullRequestsApi =
            PullRequestsApiImpl(bitbucket, client, projectKey, repositorySlug)
}
