package com.atlassian.bitbucket.server.rest.commit

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.CommitHash
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.Url

internal fun BitbucketServer.relatedPullRequestsUrl(projectKey: String,
                                                    repoSlug: String,
                                                    commitHash: CommitHash): Url =
        apiBaseUrl.resolve("rest/api/1.0/projects/$projectKey/repos/$repoSlug/commits/$commitHash/pull-requests")
