package com.atlassian.bitbucket.server.rest.user

import com.atlassian.bitbucket.server.rest.common.Links

data class User(val name: String,
                val username: String,
                val links: Links)
