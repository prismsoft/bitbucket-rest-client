package com.atlassian.bitbucket.server.rest.common

import java.net.URI

typealias Links = Map<String, Link>

data class Link(val href: URI)
