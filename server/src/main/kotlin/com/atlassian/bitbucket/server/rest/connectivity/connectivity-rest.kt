package com.atlassian.bitbucket.server.rest.connectivity

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.Url

internal fun BitbucketServer.applicationPropertiesUrl(): Url = apiBaseUrl.resolve("rest/api/1.0/application-properties")

internal fun BitbucketServer.inboxPullRequestsCountUrl(): Url = apiBaseUrl.resolve("rest/api/1.0/inbox/pull-requests/count")

internal data class InboxPullRequestsCountBean(val count: Int)

internal data class ApplicationPropertiesBean(val version: String,
                                              val buildNumber: String,
                                              val buildDate: String,
                                              val displayName: String)
