package com.atlassian.bitbucket.server.rest.personalaccesstoken

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.personalaccesstoken.Permission.*
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.Url

internal fun BitbucketServer.personalAccessTokensUrl(username: String): Url =
        apiBaseUrl.resolve("rest/access-tokens/1.0/users/$username")

internal data class CreatePersonalAccessTokenBean(val name: String,
                                                  val permissions: Iterable<String>)

internal data class PersonalAccessTokenBean(val id: String,
                                            val name: String,
                                            val token: String,
                                            val permissions: List<String>) {
    fun toPersonalAccessToken() = PersonalAccessToken(
            id = id,
            name = name,
            token = token,
            permissions = permissions.parsePermissions()
    )
}

internal data class PersonalAccessTokenInfoBean(val id: String,
                                                val name: String,
                                                val permissions: List<String>) {
    fun toPersonalAccessTokenInfo() = PersonalAccessTokenInfo(
            id = id,
            name = name,
            permissions = permissions.parsePermissions()
    )
}

private fun List<String>.parsePermissions() =
        map {
            val accessLevel = AccessLevel.valueOf(it.substringAfterLast('_'))
            val entity = it.substringBeforeLast('_')
            when (entity) {
                PROJECT_PERMISSION_PREFIX -> ProjectPermission(accessLevel)
                REPOSITORY_PERMISSION_PREFIX -> RepositoryPermission(accessLevel)
                else -> UnknownEntityPermission(entity, accessLevel)
            }
        }
