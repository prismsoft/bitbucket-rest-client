package com.atlassian.bitbucket.server.rest.pullrequest

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.BitbucketServerApiImpl
import com.atlassian.bitbucket.server.rest.common.mapErrors
import com.atlassian.bitbucket.server.rest.pagination.PagedBean
import com.atlassian.bitbucket.server.rest.pagination.ServerPage
import com.atlassian.bitbucket.server.rest.pagination.ServerPaginationApi
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import io.ktor.http.toURI
import java.net.URI

internal class PullRequestCommentsApiImpl(private val bitbucket: BitbucketServer,
                                          private val client: HttpClient,
                                          private val projectKey: String,
                                          private val repositorySlug: String,
                                          private val pullRequestId: Long) : PullRequestCommentsApi {
    private var filePath: String? = null

    override fun withFile(path: String): PullRequestCommentsApiImpl {
        filePath = path
        return this
    }

    override suspend fun page(): ServerPage<Comment> {
        val path = filePath
        return when (path) {
            null -> mapErrors("pull request activities") {
                val url = bitbucket.pullRequestActivitiesUrl(projectKey, repositorySlug, pullRequestId)
                fetchCommentsFromActivities(client, url)
            }
            else -> mapErrors("pull request comments") {
                val url = bitbucket.pullRequestFileCommentsUrl(projectKey, repositorySlug, pullRequestId, path)
                client.get<PagedBean<CommentBean>>(url)
                        .toCommentsPage(url)
            }
        }
    }
}

internal class PullRequestCommentsPagination(private val client: HttpClient,
                                             private val pageUrl: URI) : ServerPaginationApi<Comment> {

    override suspend fun nextPage(): ServerPage<Comment> {
        val url = URLBuilder().apply { takeFrom(pageUrl) }.build()
        return if (pageUrl.matchesPullRequestActivitiesUrl()) {
            mapErrors("pull request activities") {
                fetchCommentsFromActivities(client, url)
            }
        } else {
            mapErrors("pull request comments") {
                client.get<PagedBean<CommentBean>>(pageUrl.toURL())
                        .toCommentsPage(url)
            }
        }
    }
}

internal class PullRequestCommentApiImpl(private val bitbucket: BitbucketServer,
                                         private val client: HttpClient,
                                         private val projectKey: String,
                                         private val repositorySlug: String,
                                         private val pullRequestId: Long,
                                         private val commentId: Long) : PullRequestCommentApi {
    override suspend fun get(): Comment =
            mapErrors("pull request comment by ID") {
                client.get<CommentBean>(bitbucket.pullRequestCommentUrl(projectKey, repositorySlug, pullRequestId, commentId))
                        .toComment()
            }
}

private fun PagedBean<CommentBean>.toCommentsPage(url: Url) =
        toPage(url, { it.toComment() }) { it::paginateComments }

private suspend fun fetchCommentsFromActivities(client: HttpClient, startUrl: Url): ServerPage<Comment> {
    var url = startUrl
    var start = url.parameters["start"]?.toInt() ?: 0

    var pageLength = 0
    val comments = mutableListOf<Comment>()

    // Keep fetching pages until we reach the end of activities or fetch enough comments
    do {
        // Fetch activities page
        val activitiesPage = client.get<PagedBean<ActivityBean>>(url)
        // Remember page length to construct pages of the same size

        if (pageLength == 0) {
            pageLength = activitiesPage.limit
        }
        // Read all comments
        comments.addAll(activitiesPage.values
                .filter { it.action == PullRequestAction.COMMENTED }
                .map { it.toComment() })

        if (comments.size > pageLength) {
            // If we fetched more comments than needed, we need to adjust the `start` value
            // so that it points to the first comment of the next page
            val firstUnwantedCommentId = comments[pageLength].id
            val pageIndexOfFirstUnwantedComment = activitiesPage.values
                    .indexOfFirst { it.comment?.id == firstUnwantedCommentId }
            start += pageIndexOfFirstUnwantedComment
        } else {
            // Otherwise just use the provided property if this wasn't the last page
            start = activitiesPage.nextPageStart ?: -1
        }

        // Update the next page URL if required
        if (start > 0) {
            url = URLBuilder().apply {
                takeFrom(url)
                parameters["start"] = "$start"
            }.build()
        }

    } while (comments.size < pageLength && !activitiesPage.isLastPage)

    val nextPageUrl = when {
        // Negative value denotes we reached the last page
        start < 0 -> null
        else -> url.toURI()
    }

    val paginator: (BitbucketServerApiImpl) -> (URI?) -> ServerPaginationApi<Comment>? =
            { api -> { url -> api.paginateComments(url) } }

    return ServerPage(
            pageLength = pageLength,
            items = comments.take(pageLength),
            nextPageUrl = nextPageUrl,
            paginator = { api -> paginator(api)(nextPageUrl) })
}
