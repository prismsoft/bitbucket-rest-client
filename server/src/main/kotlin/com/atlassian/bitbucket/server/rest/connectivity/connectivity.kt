package com.atlassian.bitbucket.server.rest.connectivity

import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.common.mapErrors
import io.ktor.client.HttpClient
import io.ktor.client.request.get

internal class ConnectivityTestApiImpl(private val bitbucket: BitbucketServer,
                                       private val client: HttpClient) : ConnectivityTestApi {
    override suspend fun authenticatedResource() {
        mapErrors("inbox pull request count") {
            client.get<InboxPullRequestsCountBean>(bitbucket.inboxPullRequestsCountUrl())
        }
    }

    override suspend fun unauthenticatedResource() {
        mapErrors("application properties") {
            client.get<ApplicationPropertiesBean>(bitbucket.applicationPropertiesUrl())
        }
    }
}

