package com.atlassian.bitbucket.server.rest.commit

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.atlassian.bitbucket.server.rest.Authentication
import com.atlassian.bitbucket.server.rest.BitbucketServerApi
import com.atlassian.bitbucket.server.rest.apiUrlPathEqualTo
import com.atlassian.bitbucket.server.rest.common.Link
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.server.rest.pullrequest.PullRequestState
import com.atlassian.bitbucket.server.rest.pullrequest.RefInfo
import com.atlassian.bitbucket.server.rest.pullrequest.RepositoryInfo
import com.atlassian.bitbucket.server.rest.testInstance
import com.atlassian.bitbucket.server.rest.user.User
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.okJson
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri
import java.net.URI
import java.time.Instant

private const val TITLE = "Branch that has different files added and moved to same with different content src"
private const val DESCRIPTION = "* Commiting a file 1\r\n* Deleting (renaming/removing) file 1\r\n* Moving file1\r\n* Renaming file1"
private val CREATED = Instant.parse("2016-02-15T05:15:09Z")
private val UPDATED = Instant.parse("2016-03-14T05:30:50Z")

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class CommitRelatedPullRequestsTest {

    private lateinit var api: BitbucketServerApi
    private lateinit var server: WireMockServer

    @BeforeEach
    internal fun setUp(@Wiremock wiremock: WireMockServer, @WiremockUri uri: String) {
        api = BitbucketServerApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }
        server = wiremock
    }

    @AfterEach
    internal fun tearDown() = api.close()

    @Test
    fun `get related pull requests for a commit`() {
        val json = readResourceAsString("related-pull-requests.json")
        server.stubFor(get(apiUrlPathEqualTo("/projects/aProject/repos/aSlug/commits/0e773ff/pull-requests"))
                .willReturn(okJson(json)))

        val prPage = runBlocking {
            api.repository("aProject", "aSlug")
                    .commit("0e773ff")
                    .pagePullRequests()
        }

        assertThat(prPage.pageLength).isEqualTo(25)

        val pullRequests = prPage.items
        assertThat(pullRequests, "pull requests number").hasSize(1)
        assertThat(pullRequests.single(), "pull request").isEqualTo(
                PullRequest(
                        id = 8,
                        title = TITLE,
                        description = DESCRIPTION,
                        state = PullRequestState.DECLINED,
                        createdDate = CREATED,
                        updatedDate = UPDATED,
                        destination = RefInfo(
                                id = "refs/heads/master",
                                name = "master",
                                repositoryInfo = RepositoryInfo(
                                        name = "pull-request-test-repo",
                                        slug = "pull-request-test-repo",
                                        projectKey = "QA",
                                        projectName = "QA Project"
                                ),
                                commitHash = "017caf31eca7c46eb3d1800fcac431cfa7147a01"
                        ),
                        source = RefInfo(
                                id = "refs/heads/branch_that_has_different_files_added_and_moved_to_same_with_different_content_src",
                                name = "branch_that_has_different_files_added_and_moved_to_same_with_different_content_src",
                                repositoryInfo = RepositoryInfo(
                                        name = "pull-request-test-repo",
                                        slug = "pull-request-test-repo",
                                        projectKey = "QA",
                                        projectName = "QA Project"
                                ),
                                commitHash = "915ec9c5ffe7007456ef8cfecbb4fa0f25e23938"
                        ),
                        author = User(
                                name = "PR Author",
                                username = "author",
                                links = mapOf("self" to Link(URI.create("https://bbs.example.com/users/author")))
                        ),
                        reviewers = listOf(
                                User(
                                        name = "PR Reviewer",
                                        username = "reviewer",
                                        links = mapOf("self" to Link(URI.create("https://bbs.example.com/users/reviewer")))
                                )
                        ),
                        links = mapOf("self" to Link(URI.create("https://bbs.example.com/projects/QA/repos/pull-request-test-repo/pull-requests/8")))
                )
        )
    }
}
