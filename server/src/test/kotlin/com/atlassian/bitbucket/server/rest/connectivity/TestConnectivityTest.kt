package com.atlassian.bitbucket.server.rest.connectivity

import com.atlassian.bitbucket.server.rest.*
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock.*
import io.ktor.http.HttpHeaders
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class TestConnectivityTest {

    @Test
    fun `good application properties response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("application-properties.json")
        server.stubFor(get(apiUrlEqualTo("/application-properties"))
                .willReturn(okJson(json)))

        BitbucketServerApi { instance = BitbucketServer(Url(uri)) }
                .use { api ->
                    runBlocking {
                        api.testConnectivity().unauthenticatedResource()
                    }
                }
    }

    @Test
    fun `unrecognized application properties response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("unrecognized-response.json")
        server.stubFor(get(apiUrlEqualTo("/application-properties"))
                .willReturn(okJson(json)))

        BitbucketServerApi { instance = BitbucketServer(Url(uri)) }
                .use { api ->
                    assertThrows<MalformedResponse> {
                        runBlocking {
                            api.testConnectivity().unauthenticatedResource()
                        }
                    }
                }
    }

    @Test
    fun `good inbox pull request count response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("inbox-pull-request-count.json")
        server.stubFor(get(apiUrlEqualTo("/inbox/pull-requests/count"))
                .willReturn(bitbucketUnauthorized()))
        server.stubFor(get(apiUrlEqualTo("/inbox/pull-requests/count"))
                .withBasicAuth("user", "password")
                .willReturn(okJson(json)))

        BitbucketServerApi {
            authentication = Authentication.Basic("user", "password")
            instance = BitbucketServer(Url(uri))
        }.use { api ->
            runBlocking {
                api.testConnectivity().authenticatedResource()
            }
        }
    }

    @Test
    fun `unrecognized inbox pull request count response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("unrecognized-response.json")
        server.stubFor(get(apiUrlEqualTo("/inbox/pull-requests/count"))
                .willReturn(bitbucketUnauthorized()))
        server.stubFor(get(apiUrlEqualTo("/inbox/pull-requests/count"))
                .withBasicAuth("user", "password")
                .willReturn(okJson(json)))

        BitbucketServerApi {
            authentication = Authentication.Basic("user", "password")
            instance = BitbucketServer(Url(uri))
        }.use { api ->
            assertThrows<MalformedResponse> {
                runBlocking {
                    api.testConnectivity().authenticatedResource()
                }
            }
        }
    }

    private fun bitbucketUnauthorized(): ResponseDefinitionBuilder =
            unauthorized().withHeader(HttpHeaders.WWWAuthenticate, "Basic realm=\"example.com HTTP\"")
}
