package com.atlassian.bitbucket.server.rest

import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.matching.UrlPathPattern
import com.github.tomakehurst.wiremock.matching.UrlPattern
import io.ktor.http.Url

internal fun testInstance(baseUri: String) = BitbucketServer(Url(baseUri))

internal fun apiUrlEqualTo(testUrl: String) = UrlPattern(equalTo("/rest/api/1.0$testUrl"), false)

internal fun apiUrlPathEqualTo(path: String) = UrlPathPattern(equalTo("/rest/api/1.0$path"), false)
