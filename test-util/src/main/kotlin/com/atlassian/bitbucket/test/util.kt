package com.atlassian.bitbucket.test

import java.nio.charset.StandardCharsets
import java.util.*

//@Suppress("unused") // Receiver parameter is actually used
inline fun <reified T> T.readResourceAsString(resourcePath: String): String =
        Scanner(T::class.java.getResourceAsStream(resourcePath), StandardCharsets.UTF_8.name())
                .useDelimiter("\\A")
                .next()
