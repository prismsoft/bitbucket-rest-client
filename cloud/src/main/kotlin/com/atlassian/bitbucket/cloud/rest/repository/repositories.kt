package com.atlassian.bitbucket.cloud.rest.repository

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.CommitHash
import com.atlassian.bitbucket.cloud.rest.commit.CommitApi
import com.atlassian.bitbucket.cloud.rest.commit.CommitApiImpl
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestApi
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestApiImpl
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestsApi
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestsApiImpl
import io.ktor.client.HttpClient

internal class RepositoryApiImpl(private val bitbucket: BitbucketCloud,
                                 private val client: HttpClient,
                                 private val workspaceId: String,
                                 private val repositorySlug: String) : RepositoryApi {
    override fun commit(commitHash: CommitHash): CommitApi =
            CommitApiImpl(bitbucket, client, workspaceId, repositorySlug, commitHash)

    override fun pullRequest(pullRequestId: Long): PullRequestApi =
            PullRequestApiImpl(bitbucket, client, workspaceId, repositorySlug, pullRequestId)

    override fun pullRequests(): PullRequestsApi =
            PullRequestsApiImpl(bitbucket, client, workspaceId, repositorySlug)
}
