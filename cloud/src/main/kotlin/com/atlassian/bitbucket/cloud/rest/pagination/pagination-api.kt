package com.atlassian.bitbucket.cloud.rest.pagination

import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApiImpl
import com.atlassian.bitbucket.util.pagination.Page
import com.atlassian.bitbucket.util.pagination.PaginationApi
import java.net.URI

internal typealias CloudPaginationApi<T> = PaginationApi<CloudPage<T>, BitbucketCloudApiImpl, T>

data class CloudPage<T> internal constructor(val number: Int,
                                             override val pageLength: Int,
                                             val totalSize: Int?,
                                             override val items: List<T>,
                                             override val nextPageUrl: URI?,
                                             override val paginator: (BitbucketCloudApiImpl) -> CloudPaginationApi<T>?
) : Page<T, BitbucketCloudApiImpl>
