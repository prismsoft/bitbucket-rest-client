package com.atlassian.bitbucket.cloud.rest.common

import java.net.URI

internal typealias LinksBean = Map<String, List<LinkBean>>

internal fun LinksBean.toLinks(): Links =
        flatMap { (key, value) ->
            value.map { link ->
                val suffix = link.name?.let { ".$it" } ?: ""
                "$key$suffix" to link.toLink()
            }
        }.toMap()

internal data class LinkBean(val name: String?, val href: String) {
    fun toLink(): Link {
        var uri = href
        // Bitbucket might return raw SSH links which aren't proper URIs
        if (href.startsWith("git@") || href.startsWith("hg@")) {
            uri = "ssh://$href"
        }
        return Link(URI.create(uri))
    }
}
