package com.atlassian.bitbucket.cloud.rest.oauth

import com.atlassian.bitbucket.cloud.rest.close
import com.atlassian.bitbucket.cloud.rest.common.EndpointErrorInfo
import com.atlassian.bitbucket.cloud.rest.common.ResponseErrorInfo
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.cloud.rest.config.configureSerialization
import com.atlassian.bitbucket.util.submitForm
import io.ktor.client.HttpClient
import io.ktor.client.features.defaultRequest
import io.ktor.client.request.header
import io.ktor.http.HttpHeaders
import io.ktor.http.Parameters
import java.time.Instant
import java.util.*

// TODO test this
internal class BitbucketCloudOAuthApiImpl(customConfig: BitbucketCloudOAuthApiConfig.() -> Unit) : BitbucketCloudOAuthApi {

    private val config = BitbucketCloudOAuthApiConfig().apply(customConfig)
    private val httpClient: HttpClient

    init {
        with(config) {
            httpClient = httpClientBuilder().config {
                defaultRequest {
                    val authString = "${consumer.id}:${consumer.secret}"
                    val basicAuth = Base64.getEncoder().encodeToString(authString.toByteArray())
                    header(HttpHeaders.Authorization, "Basic $basicAuth")
                }
                configureSerialization()
            }
        }
    }

    override fun close() = config.close(httpClient)

    override suspend fun requestOAuthTokens(authorizationCode: AuthorizationCode): RefreshAndAccessTokens =
            with(config) {
                val data = Parameters.build {
                    append("grant_type", "authorization_code")
                    append("code", authorizationCode)
                }

                val requestTimestamp = Instant.now()
                httpClient.submitForm<OAuthTokensBean>(instance.accessTokenUrl(), data)
                        .toOAuthTokens(requestTimestamp)
                        .also { onOAuthTokensUpdate(it) }
            }

    override suspend fun refreshOAuthTokens(refreshToken: RefreshToken): RefreshAndAccessTokens =
            with(config) {
                val data = Parameters.build {
                    append("grant_type", "refresh_token")
                    append("refresh_token", refreshToken)
                }

                val onOAuthError = { _: EndpointErrorInfo, response: ResponseErrorInfo ->
                    val (status, content) = response
                    if (content.contains("Invalid refresh_token")) {
                        throw OAuthNotConfiguredException()
                    }
                    // At this point the status is definitely non-2xx
                    throw OAuthRequestFailedException(status, content)
                }

                val requestTimestamp = Instant.now()
                mapErrors("OAuth access token", onOAuthError) {
                    httpClient.submitForm<OAuthTokensBean>(instance.accessTokenUrl(), data)
                            .toOAuthTokens(requestTimestamp)
                            .also { onOAuthTokensUpdate(it) }
                }
            }
}

internal data class OAuthTokensBean(val accessToken: String,
                                    val expiresIn: Long,
                                    val refreshToken: RefreshToken) {
    /**
     * Bitbucket returns relative expiration time, and to stay safe it's safer
     * to use time of the request when converting it to an absolute timestamp.
     */
    fun toOAuthTokens(requestTimestamp: Instant): RefreshAndAccessTokens =
            RefreshAndAccessTokens(refreshToken, AccessToken(accessToken, requestTimestamp.plusSeconds(expiresIn)))
}
