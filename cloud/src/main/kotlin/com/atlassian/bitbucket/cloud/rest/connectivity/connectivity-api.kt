package com.atlassian.bitbucket.cloud.rest.connectivity

// TODO specify throws
interface ConnectivityTestApi {

    suspend fun authenticatedResource()

    suspend fun unauthenticatedResource()
}
