package com.atlassian.bitbucket.cloud.rest.snippet

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.util.submitFormWithBinaryData
import io.ktor.client.HttpClient
import io.ktor.client.request.forms.formData
import io.ktor.http.headersOf
import kotlinx.io.streams.asInput
import java.io.IOException
import java.io.InputStream
import java.util.Locale.ENGLISH
import kotlin.collections.set

internal class SnippetsApiImpl(private val bitbucket: BitbucketCloud,
                               private val client: HttpClient) : SnippetsApi {
    override fun create(workspaceId: String?): SnippetCreationApi = SnippetCreationApiImpl(bitbucket, client, workspaceId)
}

internal class SnippetCreationApiImpl(private val bitbucket: BitbucketCloud,
                                      private val client: HttpClient,
                                      private val workspaceId: String? = null) : SnippetCreationApi {
    private val files: MutableMap<String, () -> InputStream> = mutableMapOf()
    private var access: Access = Access.PRIVATE
    private var scm: Scm = Scm.GIT
    private var title: String? = null

    override fun withAccess(access: Access): SnippetCreationApi {
        this.access = access
        return this
    }

    override fun withScm(scm: Scm): SnippetCreationApi {
        this.scm = scm
        return this
    }

    override fun withFile(name: String, content: () -> InputStream): SnippetCreationApi {
        files[name] = content
        return this
    }

    override fun withTitle(title: String): SnippetCreationApi {
        this.title = title
        return this
    }

    override suspend fun save(): Snippet {
        val createSnippetUrl = when (workspaceId) {
            null -> bitbucket.createSnippetUrl()
            else -> bitbucket.createSnippetForUserUrl(workspaceId)
        }

        val inputStreams = mutableListOf<InputStream>()
        val data = formData {
            title?.let { append("title", it) }
            append("scm", scm.name.toLowerCase(ENGLISH))
            append("is_private", "${access == Access.PRIVATE}")

            files.forEach { (name, content) ->
                appendInput("file", headersOf(
                        "Content-Disposition", "filename=\"$name\"")) {
                    content().also { inputStreams.add(it) }.asInput()
                }
            }
        }

        val allFiles = AutoCloseable { inputStreams.forEach { it.closeQuietly() } }

        return allFiles.use {
            mapErrors("create snippet") {
                client.submitFormWithBinaryData<SnippetBean>(createSnippetUrl, data)
                        .toSnippet()
            }
        }
    }
}

private fun InputStream.closeQuietly() {
    try {
        close()
    } catch (ignored: IOException) {
    }
}
