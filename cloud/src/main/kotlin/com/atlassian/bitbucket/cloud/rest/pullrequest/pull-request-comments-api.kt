package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.user.User
import java.time.Instant

interface PullRequestCommentApi {
    suspend fun get(): Comment
}

interface PullRequestCommentsApi {
    suspend fun page(): CloudPage<Comment>
}

data class Comment(val id: Long,
                   val content: CommentContent,
                   val createdDate: Instant,
                   val updatedDate: Instant,
                   val author: User?,
                   val anchor: CommentAnchor?)

data class CommentContent(val html: String,
                          val markup: String,
                          val raw: String)

// TODO general/file/line anchors
data class CommentAnchor(val filePath: String,
                         val sourceLineNumber: Int?,
                         val destinationLineNumber: Int?,
                         val orphaned: Boolean)
