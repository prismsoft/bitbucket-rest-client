package com.atlassian.bitbucket.cloud.rest.commit

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.CommitHash
import com.atlassian.bitbucket.cloud.rest.common.Fields
import com.atlassian.bitbucket.cloud.rest.common.paged
import com.atlassian.bitbucket.cloud.rest.common.queryParam
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom

internal fun BitbucketCloud.relatedPullRequestsUrl(workspaceId: String,
                                                   repoSlug: String,
                                                   commitHash: CommitHash): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/commit/$commitHash/pullrequests"))
            parameters.append("fields", Fields.PullRequest.paged().queryParam())
        }.build()
