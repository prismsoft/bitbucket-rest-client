package com.atlassian.bitbucket.cloud.rest.commit

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.CommitHash
import com.atlassian.bitbucket.cloud.rest.MalformedResponse
import com.atlassian.bitbucket.cloud.rest.common.EndpointErrorInfo
import com.atlassian.bitbucket.cloud.rest.common.ResponseErrorInfo
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.pagination.PagedBean
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestBean
import com.atlassian.bitbucket.cloud.rest.pullrequest.toPullRequestsPage
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse

internal class CommitApiImpl(private val bitbucket: BitbucketCloud,
                             private val client: HttpClient,
                             private val workspaceId: String,
                             private val repositorySlug: String,
                             private val commitHash: CommitHash) : CommitApi {

    override suspend fun pagePullRequests(): CloudPage<PullRequest> {
        val onAddOnNotFound: (EndpointErrorInfo, ResponseErrorInfo) -> Unit = { _, response: ResponseErrorInfo ->
            when (response.status.value) {
                400, 404 -> throw PullRequestLinksNotEnabledException(workspaceId, repositorySlug)
            }
        }

        return mapErrors("commit pull requests", onAddOnNotFound) {
            val response = client.get<HttpResponse>(bitbucket.relatedPullRequestsUrl(workspaceId, repositorySlug, commitHash))
            when (response.status.value) {
                200 -> response.call.receive<PagedBean<PullRequestBean>>()
                        .toPullRequestsPage()
                202 -> throw PullRequestLinksIndexingException(workspaceId, repositorySlug)
                201, in 203..299 -> throw MalformedResponse("Unexpected response code ${response.status.value} for commit pull requests")
                // Continue receiving to trigger error handling
                else -> response.call.receive()
            }
        }
    }
}

