package com.atlassian.bitbucket.cloud.rest.snippet

import com.atlassian.bitbucket.cloud.rest.common.Links
import com.atlassian.bitbucket.cloud.rest.user.Owner
import com.atlassian.bitbucket.cloud.rest.user.User
import java.io.InputStream
import java.time.Instant

interface SnippetsApi {
    fun create(workspaceId: String? = null): SnippetCreationApi
}

interface SnippetCreationApi {
    fun withAccess(access: Access): SnippetCreationApi
    fun withScm(scm: Scm): SnippetCreationApi
    fun withFile(name: String, content: () -> InputStream): SnippetCreationApi
    fun withTitle(title: String): SnippetCreationApi

    suspend fun save(): Snippet
}

enum class Access { PRIVATE, PUBLIC }
enum class Scm { GIT, HG }

data class Snippet(val id: String,
                   val title: String,
                   val creator: User?,
                   val owner: Owner,
                   val scm: Scm,
                   val isPrivate: Boolean,
                   val createdDate: Instant,
                   val updatedDate: Instant,
                   val files: Map<String, Links>,
                   val links: Links)
