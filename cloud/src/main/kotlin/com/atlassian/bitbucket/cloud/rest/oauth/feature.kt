package com.atlassian.bitbucket.cloud.rest.oauth

import com.atlassian.bitbucket.cloud.rest.oauth.OAuth.TokensMessage.GetTokens
import com.atlassian.bitbucket.cloud.rest.oauth.OAuth.TokensMessage.RefreshTokens
import io.ktor.client.HttpClient
import io.ktor.client.call.HttpClientCall
import io.ktor.client.features.HttpClientFeature
import io.ktor.client.features.HttpSend
import io.ktor.client.features.Sender
import io.ktor.client.features.feature
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.HttpRequestPipeline
import io.ktor.client.request.takeFrom
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.OutgoingContent
import io.ktor.util.AttributeKey
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.time.withTimeout
import java.time.Duration

private const val BEARER_PREFIX = "Bearer "

// TODO test this
class OAuth(private val oAuthApi: BitbucketCloudOAuthApi,
            private val refreshTokensTimeout: Duration,
            private val oAuthTokens: OAuthTokens) {

    class Configuration {

        /**
         * Required: Bitbucket Cloud OAuth API instance to request and refresh access token.
         */
        lateinit var oAuthApi: BitbucketCloudOAuthApi

        /**
         * Required: OAuth refresh token is mandatory while access token is optional.
         */
        lateinit var oAuthTokens: OAuthTokens

        /**
         * Timeout of refresh OAuth tokens operation, 10 seconds by default.
         */
        var refreshTokensTimeout: Duration = Duration.ofSeconds(10)

        internal fun build() = OAuth(oAuthApi, refreshTokensTimeout, oAuthTokens)
    }

    internal sealed class TokensMessage {
        internal class RefreshTokens(val oldTokens: OAuthTokens) : TokensMessage()
        internal class GetTokens(val response: CompletableDeferred<OAuthTokens>) : TokensMessage()
    }

    @UseExperimental(ObsoleteCoroutinesApi::class)
    internal fun refreshTokenActor(scope: CoroutineScope) =
            scope.actor<TokensMessage> {
                var tokens = oAuthTokens
                for (message in channel) { // iterate over incoming messages
                    when (message) {
                        is RefreshTokens -> {
                            if (message.oldTokens == tokens) {
                                tokens = oAuthApi.refreshOAuthTokens(tokens.refreshToken)
                            }
                        }
                        is GetTokens -> message.response.complete(tokens)
                    }
                }
            }

    @UseExperimental(KtorExperimentalAPI::class)
    companion object Feature : HttpClientFeature<Configuration, OAuth> {
        override val key: AttributeKey<OAuth> = AttributeKey("OAuthHeader")

        private lateinit var tokensActor: SendChannel<TokensMessage>
        private lateinit var refreshTokensTimeout: Duration

        override fun prepare(block: Configuration.() -> Unit): OAuth = Configuration().apply(block).build()

        override fun install(feature: OAuth, scope: HttpClient) {
            tokensActor = feature.refreshTokenActor(scope)
            refreshTokensTimeout = feature.refreshTokensTimeout

            // Add `Authorization` header to the request when suitable
            scope.requestPipeline.intercept(HttpRequestPipeline.State) {
                if (context.headers.getAll(HttpHeaders.Authorization) != null) return@intercept

                context.configureOAuth(tokensForCall().accessToken)
            }

            // Handle response to catch the case of invalidated or just expired access token
            scope.feature(HttpSend)?.intercept { origin -> handleCall(origin) }
        }


        private suspend fun Sender.handleCall(origin: HttpClientCall): HttpClientCall {
            // Only react on 401 Unauthorized
            if (origin.response.status != HttpStatusCode.Unauthorized) return origin

            // Parse the access token used for the request
            val requestAccessToken = origin.request.headers[HttpHeaders.Authorization]
                    ?.let { tokenFromAuthorizationHeader(it) } ?: return origin

            // Try to refresh access token to either get the new one or raise an error indicating
            // that OAuth is not configured (in case the token was revoked on remote)
            val tokens = tokensForCall(requestAccessToken)

            return when (origin.request.content) {
                // We can retry a request if it had empty body
                is OutgoingContent.NoContent -> execute(HttpRequestBuilder().apply {
                    takeFrom(origin.request)
                    configureOAuth(tokens.accessToken)
                })
                // Otherwise we might be not able to send the body again, but since the access token
                // has been refreshed successfully this request can potentially be retried by the caller,
                // so we throw an exception indicating that
                else -> throw OAuthAccessTokenLateRefreshException()
            }
        }

        private suspend fun getTokens(): OAuthTokens {
            val response = CompletableDeferred<OAuthTokens>()
            tokensActor.send(GetTokens(response))
            return response.await()
        }

        private suspend fun refreshTokens(oldTokens: OAuthTokens): RefreshAndAccessTokens =
                try {
                    withTimeout(refreshTokensTimeout) {
                        tokensActor.send(RefreshTokens(oldTokens))
                        getTokens() as RefreshAndAccessTokens
                    }
                } catch (e: TimeoutCancellationException) {
                    throw OAuthException("Timeout when refreshing OAuth access code", e)
                }


        private suspend fun tokensForCall(previousCallAccessToken: String? = null): RefreshAndAccessTokens =
                when (val currentTokens = getTokens()) {
                    is RefreshTokenOnly -> refreshTokens(currentTokens)
                    is RefreshAndAccessTokens ->
                        // Refresh tokens only if they haven't been refreshed by someone else since previous call
                        if (currentTokens.accessToken.isExpired() ||
                                previousCallAccessToken == currentTokens.accessToken.value) {
                            refreshTokens(currentTokens)
                        } else {
                            currentTokens
                        }
                }

        private fun HttpRequestBuilder.configureOAuth(accessToken: AccessToken) {
            headers[HttpHeaders.Authorization] = "$BEARER_PREFIX${accessToken.value}"
        }

        private fun tokenFromAuthorizationHeader(header: String) =
                header.takeIf { it.startsWith(BEARER_PREFIX) }?.substringAfter(BEARER_PREFIX)
    }
}
