package com.atlassian.bitbucket.cloud.rest.pagination

import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApiImpl
import com.fasterxml.jackson.annotation.JsonProperty
import java.net.URI

internal data class PagedBean<T>(@JsonProperty("page") val number: Int,
                                 @JsonProperty("pagelen") val length: Int,
                                 val size: Int?,
                                 val next: String? = null,
                                 val values: List<T> = emptyList()) {
    fun <R> toPage(valueConverter: (T) -> R,
                   paginator: (BitbucketCloudApiImpl) -> (URI?) -> CloudPaginationApi<R>?): CloudPage<R> {
        val nextPageUrl = next?.let { URI.create(it) }
        return CloudPage(
                number = number,
                pageLength = length,
                totalSize = size,
                items = values.map { valueConverter(it) },
                nextPageUrl = nextPageUrl,
                paginator = { apiProvider -> paginator(apiProvider)(nextPageUrl) }
        )
    }
}
