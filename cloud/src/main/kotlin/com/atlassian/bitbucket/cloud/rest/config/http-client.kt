package com.atlassian.bitbucket.cloud.rest.config

import com.atlassian.bitbucket.cloud.rest.MalformedResponse
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import io.ktor.client.HttpClientConfig
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import java.time.Instant
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.*

private val bitbucketDateFormat = DateTimeFormatterBuilder()
        .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
        .appendFraction(ChronoField.MICRO_OF_SECOND, 6, 6, true)
        .appendOffsetId()
        .toFormatter(Locale.ENGLISH)

fun HttpClientConfig<*>.configureSerialization() {
    install(JsonFeature) {
        serializer = JacksonSerializer {
            enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
            enable(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES)
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

            propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE

            registerModule(SimpleModule()
                    .addDeserializer(Instant::class.java, object : JsonDeserializer<Instant>() {
                        override fun deserialize(p: JsonParser, ctx: DeserializationContext) =
                                p.text?.let { Instant.from(bitbucketDateFormat.parse(p.text)) }
                                        ?: throw MalformedResponse("Failed to parse timestamp from '${p.currentName}: ${p.text}'")
                    })
                    .addSerializer(Instant::class.java, object : JsonSerializer<Instant>() {
                        override fun serialize(value: Instant, json: JsonGenerator, serializers: SerializerProvider) =
                                json.writeString(bitbucketDateFormat.format(value))
                    }))
        }
    }
}
