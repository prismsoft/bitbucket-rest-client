package com.atlassian.bitbucket.cloud.rest.connectivity

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.cloud.rest.user.UsersApiImpl
import io.ktor.client.HttpClient
import io.ktor.client.request.get

internal class ConnectivityTestApiImpl(private val bitbucket: BitbucketCloud,
                                       private val client: HttpClient) : ConnectivityTestApi {
    override suspend fun authenticatedResource() {
        UsersApiImpl(bitbucket, client).self()
    }

    override suspend fun unauthenticatedResource() {
        mapErrors("webhook events") {
            client.get<WebHookEventsBean>(bitbucket.webhookEventsUrl())
        }
    }
}

