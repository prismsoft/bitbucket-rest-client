package com.atlassian.bitbucket.cloud.rest.common

typealias FieldList = List<String>

internal object Fields {
    val PullRequest: FieldList = listOf(
            "author.account_id",
            "author.display_name",
            "author.links.avatar.href",
            "author.nickname",
            "author.type",
            "links.html.href",
            "id",
            "title",
            "state",
            "description",
            "created_on",
            "updated_on",
            "destination.branch.name",
            "destination.commit.hash",
            "destination.repository.name",
            "destination.repository.full_name",
            "source.branch.name",
            "source.commit.hash",
            "source.repository.name",
            "source.repository.full_name",
            "reviewers.account_id",
            "reviewers.display_name",
            "reviewers.links.avatar.href",
            "reviewers.nickname",
            "reviewers.type")

    val Comment: FieldList = listOf(
            "id",
            "content.html",
            "content.markup",
            "content.raw",
            "created_on",
            "updated_on",
            "user.links.avatar.href",
            "user.account_id",
            "user.display_name",
            "user.nickname",
            "inline.to",
            "inline.from",
            "inline.path")
}

private val Page: FieldList = listOf(
        "page",
        "pagelen",
        "size",
        "previous",
        "next"
)

internal fun FieldList.paged(): FieldList = Page + map { "values.$it" }

internal fun FieldList.queryParam() = joinToString(separator = ",")
