package com.atlassian.bitbucket.cloud.rest.user

import com.atlassian.bitbucket.cloud.rest.common.Links

interface UsersApi {
    suspend fun self(): User
}

sealed class Owner(val id: String,
                   open val links: Links)

data class User(val name: String,
                val nickname: String,
                val accountId: String,
                override val links: Links) : Owner(accountId, links)

data class Team(val name: String,
                val username: String,
                override val links: Links) : Owner(username, links)
