package com.atlassian.bitbucket.cloud.rest.connectivity

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.LinksBean
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.Url

internal fun BitbucketCloud.webhookEventsUrl(): Url = apiBaseUrl.resolve("2.0/hook_events")

internal data class WebHookEventsBean(val user: WebHookSubjectBean,
                                      val repository: WebHookSubjectBean,
                                      val team: WebHookSubjectBean)

internal data class WebHookSubjectBean(val links: LinksBean)
