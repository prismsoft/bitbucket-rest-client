package com.atlassian.bitbucket.cloud.rest.user

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.LinksBean
import com.atlassian.bitbucket.cloud.rest.common.toLinks
import com.atlassian.bitbucket.util.resolve
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id
import io.ktor.http.Url

fun BitbucketCloud.userUrl(): Url = apiBaseUrl.resolve("2.0/user")

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = TeamBean::class, name = "team"),
        JsonSubTypes.Type(value = UserBean::class, name = "user")
)
internal sealed class OwnerBean {
    abstract fun toOwner(): Owner
}

internal data class TeamBean(val displayName: String,
                             val username: String,
                             val links: LinksBean) : OwnerBean() {
    override fun toOwner() = toTeam()

    fun toTeam() = Team(displayName, username, links.toLinks())
}

internal data class UserBean(val displayName: String,
                             val accountId: String,
                             val nickname: String,
                             val links: LinksBean) : OwnerBean() {
    override fun toOwner() = toUser()

    fun toUser() = User(displayName, nickname, accountId, links.toLinks())
}
