package com.atlassian.bitbucket.cloud.rest.oauth

import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApiCommonConfig
import com.atlassian.bitbucket.cloud.rest.BitbucketCloudRestClientException
import io.ktor.http.HttpStatusCode
import kotlinx.io.core.Closeable
import java.time.Instant

typealias AuthorizationCode = String
typealias RefreshToken = String

data class AccessToken(val value: String, val expiresAt: Instant) {
    fun isExpired() = Instant.now().isAfter(expiresAt)
}

data class OAuthConsumer(val id: String, val secret: String)

interface BitbucketCloudOAuthApi : Closeable {
    suspend fun requestOAuthTokens(authorizationCode: AuthorizationCode): RefreshAndAccessTokens

    suspend fun refreshOAuthTokens(refreshToken: RefreshToken): RefreshAndAccessTokens

    companion object {
        operator fun invoke(config: BitbucketCloudOAuthApiConfig.() -> Unit = { }): BitbucketCloudOAuthApi =
                BitbucketCloudOAuthApiImpl(config)
    }
}

class BitbucketCloudOAuthApiConfig : BitbucketCloudApiCommonConfig() {
    /**
     * OAuth consumer to authenticate with.
     */
    lateinit var consumer: OAuthConsumer

    /**
     * Callback to invoke when OAuth tokens are refreshed.
     */
    var onOAuthTokensUpdate: (RefreshAndAccessTokens) -> Unit = { httpClientBuilder }
}


sealed class OAuthTokens(open val refreshToken: RefreshToken)
data class RefreshTokenOnly(override val refreshToken: RefreshToken) : OAuthTokens(refreshToken)

// TODO accept with both strings, look at inline class
data class RefreshAndAccessTokens(override val refreshToken: RefreshToken,
                                  val accessToken: AccessToken) : OAuthTokens(refreshToken)

/**
 * Parent class for all OAuth-related exceptions.
 */
open class OAuthException(message: String, cause: Throwable? = null) : BitbucketCloudRestClientException(message, cause)

/**
 * Bitbucket rejected provided OAuth refresh token, configuration change is required.
 */
class OAuthNotConfiguredException : OAuthException("OAuth is not configured")

/**
 * Bitbucket responded with unexpected status code to an OAuth request.
 */
class OAuthRequestFailedException(status: HttpStatusCode, val body: String? = null) :
        OAuthException("Failed to request OAuth access code, HTTP status ${status.value} (${status.description})")

/**
 * Bitbucket responded with 401 (Unauthorized) to a request which was authenticated
 * with an access token that was supposed to be valid. It has been refreshed after
 * the failed request, but that request can't be retried automatically as it had
 * a non-empty body. The caller might be able to retry the request.
 */
class OAuthAccessTokenLateRefreshException :
        OAuthException("OAuth access token happened to be out of date, request can't be retried automatically")
