package com.atlassian.bitbucket.cloud.rest.snippet

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.LinksBean
import com.atlassian.bitbucket.cloud.rest.common.toLinks
import com.atlassian.bitbucket.cloud.rest.user.OwnerBean
import com.atlassian.bitbucket.cloud.rest.user.UserBean
import com.atlassian.bitbucket.util.resolve
import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.http.Url
import java.time.Instant

internal fun BitbucketCloud.createSnippetUrl(): Url = apiBaseUrl.resolve("2.0/snippets")
internal fun BitbucketCloud.createSnippetForUserUrl(accountId: String): Url = apiBaseUrl.resolve("2.0/snippets/$accountId")

internal data class SnippetBean(val id: String,
                                val title: String,
                                val creator: UserBean?,
                                val owner: OwnerBean,
                                val scm: String,
                                val isPrivate: Boolean,
                                @JsonProperty("created_on") val createdDate: Instant,
                                @JsonProperty("updated_on") val updatedDate: Instant,
                                val files: Map<String, FileBean>,
                                val links: LinksBean) {
    fun toSnippet() = Snippet(
            id = id,
            title = title,
            creator = creator?.toUser(),
            owner = owner.toOwner(),
            scm = Scm.valueOf(scm.toUpperCase()),
            isPrivate = isPrivate,
            createdDate = createdDate,
            updatedDate = updatedDate,
            files = files.mapValues { it.value.links.toLinks() },
            links = links.toLinks()
    )
}

internal data class FileBean(val links: LinksBean)

