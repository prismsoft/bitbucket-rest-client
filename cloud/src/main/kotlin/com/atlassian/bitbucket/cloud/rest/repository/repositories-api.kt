package com.atlassian.bitbucket.cloud.rest.repository

import com.atlassian.bitbucket.cloud.rest.CommitHash
import com.atlassian.bitbucket.cloud.rest.commit.CommitApi
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestApi
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestsApi

interface RepositoryApi {
    fun commit(commitHash: CommitHash): CommitApi

    fun pullRequest(pullRequestId: Long): PullRequestApi
    fun pullRequests(): PullRequestsApi
}
