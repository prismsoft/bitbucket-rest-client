package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPaginationApi
import com.atlassian.bitbucket.cloud.rest.pagination.PagedBean
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import java.net.URI

internal class PullRequestCommentsApiImpl(private val bitbucket: BitbucketCloud,
                                          private val client: HttpClient,
                                          private val workspaceId: String,
                                          private val repositorySlug: String,
                                          private val pullRequestId: Long) : PullRequestCommentsApi {
    override suspend fun page(): CloudPage<Comment> =
            mapErrors("pull request comments") {
                client.get<PagedBean<CommentBean>>(bitbucket.pullRequestCommentsUrl(workspaceId, repositorySlug, pullRequestId))
                        .toCommentsPage()

            }
}

internal class PullRequestCommentsPagination(private val client: HttpClient,
                                             private val pageUrl: URI) : CloudPaginationApi<Comment> {
    override suspend fun nextPage(): CloudPage<Comment> =
            mapErrors("pull request comments") {
                client.get<PagedBean<CommentBean>>(pageUrl.toURL())
                        .toCommentsPage()

            }
}

internal class PullRequestCommentApiImpl(private val bitbucket: BitbucketCloud,
                                         private val client: HttpClient,
                                         private val workspaceId: String,
                                         private val repositorySlug: String,
                                         private val pullRequestId: Long,
                                         private val commentId: Long) : PullRequestCommentApi {
    override suspend fun get(): Comment =
            mapErrors("pull request comment by ID") {
                client.get<CommentBean>(bitbucket.pullRequestCommentUrl(workspaceId, repositorySlug, pullRequestId, commentId))
                        .toComment()
            }
}

private fun PagedBean<CommentBean>.toCommentsPage() =
        toPage({ it.toComment() }) { it::paginateComments }
