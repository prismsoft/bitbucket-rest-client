package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPaginationApi
import com.atlassian.bitbucket.cloud.rest.pagination.PagedBean
import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.post
import java.net.URI

internal open class PullRequestsApiImpl(private val bitbucket: BitbucketCloud,
                                        private val client: HttpClient,
                                        private val workspaceId: String,
                                        private val repositorySlug: String) : PullRequestsApi {
    private var authorNickname: String? = null
    private var reviewerNickname: String? = null
    private var state: PullRequestState? = null
    private var targetBranchName: String? = null

    override fun withAuthor(nickname: String): PullRequestsApi {
        authorNickname = nickname
        return this
    }

    override fun withReviewer(nickname: String): PullRequestsApi {
        reviewerNickname = nickname
        return this
    }

    override fun withState(state: PullRequestState): PullRequestsApi {
        this.state = state
        return this
    }

    override fun withTargetBranch(branchName: String): PullRequestsApi {
        targetBranchName = branchName
        return this
    }

    override suspend fun page(): CloudPage<PullRequest> {
        val bbqlQuery = listOfNotNull(
                authorNickname?.let { "author.nickname=\"$it\"" },
                reviewerNickname?.let { "reviewers.nickname=\"$it\"" },
                state?.let { "state=\"$it\"" },
                targetBranchName?.let { "destination.branch.name=\"$it\"" }
        ).joinToString(" AND ")
                .takeIf { it.isNotBlank() }

        return mapErrors("pull requests") {
            client.get<PagedBean<PullRequestBean>>(bitbucket.pullRequestsUrl(workspaceId, repositorySlug, bbqlQuery))
                    .toPullRequestsPage()
        }
    }
}

internal class PullRequestsPagination(private val client: HttpClient,
                                      private val pageUrl: URI) : CloudPaginationApi<PullRequest> {
    override suspend fun nextPage(): CloudPage<PullRequest> =
            mapErrors("pull requests") {
                client.get<PagedBean<PullRequestBean>>(pageUrl.toURL())
                        .toPullRequestsPage()
            }
}

internal class PullRequestApiImpl(private val bitbucket: BitbucketCloud,
                                  private val client: HttpClient,
                                  private val workspaceId: String,
                                  private val repositorySlug: String,
                                  private val id: Long) : PullRequestApi {
    override fun comment(commentId: Long): PullRequestCommentApi =
            PullRequestCommentApiImpl(bitbucket, client, workspaceId, repositorySlug, id, commentId)

    override fun comments(): PullRequestCommentsApi =
            PullRequestCommentsApiImpl(bitbucket, client, workspaceId, repositorySlug, id)


    override suspend fun get(): PullRequest =
            mapErrors("pull request by ID") {
                client.get<PullRequestBean>(bitbucket.pullRequestUrl(workspaceId, repositorySlug, id))
                        .toPullRequest()
            }

    override suspend fun approve() =
            mapErrors("approve pull request") {
                client.post<Unit>(bitbucket.approvePullRequestUrl(workspaceId, repositorySlug, id))
            }

    override suspend fun unapprove() =
            mapErrors("unapprove pull request") {
                client.delete<Unit>(bitbucket.approvePullRequestUrl(workspaceId, repositorySlug, id))
            }
}

internal fun PagedBean<PullRequestBean>.toPullRequestsPage() =
        toPage({ it.toPullRequest() }) { it::paginatePullRequests }
