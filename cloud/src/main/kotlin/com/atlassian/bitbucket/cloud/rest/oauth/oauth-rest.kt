package com.atlassian.bitbucket.cloud.rest.oauth

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.Url

internal fun BitbucketCloud.accessTokenUrl(): Url =
        baseUrl.resolve("site/oauth2/access_token")
