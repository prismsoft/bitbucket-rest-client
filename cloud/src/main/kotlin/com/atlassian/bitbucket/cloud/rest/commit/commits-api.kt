package com.atlassian.bitbucket.cloud.rest.commit

import com.atlassian.bitbucket.cloud.rest.BitbucketCloudRestClientException
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequest

interface CommitApi {
    suspend fun pagePullRequests(): CloudPage<PullRequest>
}

class PullRequestLinksNotEnabledException(workspaceId: String, repositorySlug: String)
    : BitbucketCloudRestClientException("Pull Request commit links are not indexed in '$workspaceId/$repositorySlug' repository")


class PullRequestLinksIndexingException(workspaceId: String, repositorySlug: String)
    : BitbucketCloudRestClientException("Pull Request commit links are being indexed in '$workspaceId/$repositorySlug' repository")
