package com.atlassian.bitbucket.cloud.rest

import com.atlassian.bitbucket.HttpClientLifecyclePolicy
import com.atlassian.bitbucket.cloud.rest.Authentication.Guest
import com.atlassian.bitbucket.cloud.rest.connectivity.ConnectivityTestApi
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthConsumer
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthTokens
import com.atlassian.bitbucket.cloud.rest.oauth.RefreshAndAccessTokens
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.repository.RepositoryApi
import com.atlassian.bitbucket.cloud.rest.snippet.SnippetsApi
import com.atlassian.bitbucket.cloud.rest.user.UsersApi
import io.ktor.client.HttpClient
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol.Companion.HTTPS
import io.ktor.http.Url
import io.ktor.http.takeFrom
import kotlinx.io.core.Closeable
import java.net.URI

typealias CommitHash = String

interface BitbucketCloudApi : Closeable {
    fun repository(workspaceId: String, slug: String): RepositoryApi
    fun snippets(): SnippetsApi
    fun users(): UsersApi

    suspend fun <T> nextPage(page: CloudPage<T>): CloudPage<T>?

    fun testConnectivity(): ConnectivityTestApi

    companion object {
        operator fun invoke(config: BitbucketCloudApiConfig.() -> Unit = { }): BitbucketCloudApi =
                BitbucketCloudApiImpl(config)
    }
}

open class BitbucketCloudApiCommonConfig {
    internal var httpClientBuilder: () -> HttpClient = { HttpClient() }
    internal var httpClientLifecyclePolicy: HttpClientLifecyclePolicy = HttpClientLifecyclePolicy.BOUND
    internal var onCloseAction: () -> Unit = {}

    /**
     * Instance of Bitbucket Cloud to connect to.
     */
    var instance: BitbucketCloud = BitbucketCloud.Production

    /**
     * Allows to customize the configuration of the underlying HTTP client.
     *
     * @param lifecyclePolicy lifecycle policy for the underlying HTTP client
     * @param builder function that creates an [HttpClient] instance
     */
    fun httpClient(lifecyclePolicy: HttpClientLifecyclePolicy = HttpClientLifecyclePolicy.BOUND,
                   builder: () -> HttpClient) {
        httpClientBuilder = builder
        httpClientLifecyclePolicy = lifecyclePolicy
    }

    /**
     * Custom action to run when REST API client is [kotlinx.io.core.CloseableKt][closed].
     *
     * @param block code to execute
     */
    fun onClose(block: () -> Unit) {
        onCloseAction = block
    }
}

class BitbucketCloudApiConfig : BitbucketCloudApiCommonConfig() {
    /**
     * Authentication to use when requesting Bitbucket Cloud.
     */
    var authentication: Authentication = Guest
}

sealed class BitbucketCloud(val baseUrl: Url,
                            val apiBaseUrl: Url,
                            val addOnApiBaseUrl: Url) {
    companion object {
        fun forUrl(baseUri: URI): BitbucketCloud = forUrl(URLBuilder().apply { takeFrom(baseUri) }.build())
        fun forUrl(baseUri: String): BitbucketCloud = forUrl(Url(baseUri))
        fun forUrl(baseUrl: Url): BitbucketCloud =
                when (baseUrl.host) {
                    "bitbucket.org" -> Production
                    "staging.bb-inf.net" -> Staging
                    else -> throw IllegalArgumentException("Base URL doesn't match any known Bitbucket Cloud instance")
                }
    }

    object Production : BitbucketCloud(
            URLBuilder(HTTPS, "bitbucket.org").build(),
            URLBuilder(HTTPS, "api.bitbucket.org").build(),
            URLBuilder(HTTPS, "api.bitbucket.io").build())

    object Staging : BitbucketCloud(
            URLBuilder(HTTPS, "staging.bb-inf.net").build(),
            URLBuilder(HTTPS, "api-staging.bb-inf.net").build(),
            URLBuilder(HTTPS, "api-io-staging.bb-inf.net").build())

    internal class CustomInstance(baseUrl: Url) : BitbucketCloud(
            baseUrl,
            URLBuilder().apply {
                takeFrom(baseUrl)
                encodedPath = "/!api"
            }.build(),
            baseUrl)
}

sealed class Authentication {
    object Guest : Authentication()
    data class Basic(val username: String, val password: String) : Authentication()
    data class OAuth(val consumer: OAuthConsumer,
                     val tokens: OAuthTokens,
                     val onTokensUpdate: (RefreshAndAccessTokens) -> Unit = { }) : Authentication()
}

open class BitbucketCloudRestClientException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

open class BadResponseCodeException(status: HttpStatusCode, val endpointName: String, val url: Url)
    : BitbucketCloudRestClientException("Bitbucket responded with ${status.value} (${status.description}) for $endpointName endpoint [$url]") {
    val code = status.value
    val description = status.description
}

class ForbiddenException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class UnauthorizedException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class NotFoundException(status: HttpStatusCode, endpointName: String, url: Url) : BadResponseCodeException(status, endpointName, url)

class MalformedResponse(message: String, cause: Throwable? = null) : BitbucketCloudRestClientException(message, cause)
