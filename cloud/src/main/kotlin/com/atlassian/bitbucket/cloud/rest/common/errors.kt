package com.atlassian.bitbucket.cloud.rest.common

import com.atlassian.bitbucket.cloud.rest.*
import com.fasterxml.jackson.core.JsonProcessingException
import io.ktor.client.features.ResponseException
import io.ktor.client.response.readText
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url

data class ResponseErrorInfo(val status: HttpStatusCode,
                             val content: String)

data class EndpointErrorInfo(val name: String,
                             val url: Url)

suspend fun <R> mapErrors(endpointName: String,
                          throwCustomError: (EndpointErrorInfo, ResponseErrorInfo) -> Unit = { _, _ -> },
                          block: suspend () -> R): R {

    fun genericError(e: Throwable) =
            BitbucketCloudRestClientException("Error requesting '$endpointName' API endpoint", e)

    return try {
        block()
    } catch (e: ResponseException) {
        val url = e.response.call.request.url
        // TODO parse error
        val content = e.response.readText()

        val status = e.response.status
        // Throw custom error if matched
        throwCustomError(EndpointErrorInfo(endpointName, url), ResponseErrorInfo(status, content))

        // TODO refactor exception args
        throw when (status.value) {
            401 -> UnauthorizedException(status, endpointName, url)
            403 -> ForbiddenException(status, endpointName, url)
            404 -> NotFoundException(status, endpointName, url)
            else -> BadResponseCodeException(status, endpointName, url)
        }
    } catch (e: JsonProcessingException) {
        throw MalformedResponse("Malformed response for '$endpointName' API endpoint", e)
    } catch (e: BitbucketCloudRestClientException) {
        throw e
    } catch (e: Exception) {
        throw genericError(e)
    }
}
