package com.atlassian.bitbucket.cloud.rest.user

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.mapErrors
import io.ktor.client.HttpClient
import io.ktor.client.request.get

internal class UsersApiImpl(private val bitbucket: BitbucketCloud,
                            private val client: HttpClient) : UsersApi {

    override suspend fun self(): User =
            mapErrors("user") {
                client.get<UserBean>(bitbucket.userUrl()).toUser()
            }
}
