package com.atlassian.bitbucket.cloud.rest

import com.atlassian.bitbucket.HttpClientLifecyclePolicy.UNRELATED
import com.atlassian.bitbucket.cloud.rest.config.configureSerialization
import com.atlassian.bitbucket.cloud.rest.connectivity.ConnectivityTestApi
import com.atlassian.bitbucket.cloud.rest.connectivity.ConnectivityTestApiImpl
import com.atlassian.bitbucket.cloud.rest.oauth.BitbucketCloudOAuthApi
import com.atlassian.bitbucket.cloud.rest.oauth.OAuth
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPaginationApi
import com.atlassian.bitbucket.cloud.rest.pullrequest.Comment
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestCommentsPagination
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestsPagination
import com.atlassian.bitbucket.cloud.rest.repository.RepositoryApi
import com.atlassian.bitbucket.cloud.rest.repository.RepositoryApiImpl
import com.atlassian.bitbucket.cloud.rest.snippet.SnippetsApi
import com.atlassian.bitbucket.cloud.rest.snippet.SnippetsApiImpl
import com.atlassian.bitbucket.cloud.rest.user.UsersApi
import com.atlassian.bitbucket.cloud.rest.user.UsersApiImpl
import com.atlassian.bitbucket.util.closeIfRequired
import com.atlassian.bitbucket.util.pagination.PaginationApiProvider
import io.ktor.client.HttpClient
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.basic
import java.net.URI

internal class BitbucketCloudApiImpl(customConfig: BitbucketCloudApiConfig.() -> Unit) : BitbucketCloudApi, PaginationApiProvider {

    private val config = BitbucketCloudApiConfig().apply(customConfig)
    private val httpClient: HttpClient

    init {
        with(config) {
            val client = httpClientBuilder()
            val auth = authentication
            httpClient = when (auth) {
                is Authentication.Basic -> client.config {
                    install(Auth) {
                        basic {
                            username = auth.username
                            password = auth.password
                        }
                    }
                }
                is Authentication.OAuth -> client.config {
                    install(OAuth) {
                        oAuthApi = BitbucketCloudOAuthApi {
                            instance = this@with.instance
                            consumer = auth.consumer
                            onOAuthTokensUpdate = auth.onTokensUpdate
                            // No need to bind the HTTP client another time
                            httpClient(lifecyclePolicy = UNRELATED) { client }
                        }
                        oAuthTokens = auth.tokens
                    }
                }
                is Authentication.Guest -> client
            }.config {
                configureSerialization()
            }
        }
    }

    override fun close() = config.close(httpClient)

    override fun repository(workspaceId: String, slug: String): RepositoryApi =
            RepositoryApiImpl(config.instance, httpClient, workspaceId, slug)

    override fun snippets(): SnippetsApi = SnippetsApiImpl(config.instance, httpClient)

    override fun users(): UsersApi = UsersApiImpl(config.instance, httpClient)

    override fun testConnectivity(): ConnectivityTestApi = ConnectivityTestApiImpl(config.instance, httpClient)

    override suspend fun <T> nextPage(page: CloudPage<T>): CloudPage<T>? =
            page.paginator(this)?.nextPage()

    internal fun paginatePullRequests(nextPageUrl: URI?): CloudPaginationApi<PullRequest>? =
            nextPageUrl?.let { PullRequestsPagination(httpClient, it) }

    internal fun paginateComments(nextPageUrl: URI?): CloudPaginationApi<Comment>? =
            nextPageUrl?.let { PullRequestCommentsPagination(httpClient, it) }
}

internal fun BitbucketCloudApiCommonConfig.close(httpClient: HttpClient) {
    onCloseAction()
    httpClient.closeIfRequired(httpClientLifecyclePolicy)
}
