package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.Fields
import com.atlassian.bitbucket.cloud.rest.common.paged
import com.atlassian.bitbucket.cloud.rest.common.queryParam
import com.atlassian.bitbucket.cloud.rest.user.UserBean
import com.atlassian.bitbucket.util.resolve
import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import java.time.Instant

internal fun BitbucketCloud.pullRequestCommentsUrl(workspaceId: String,
                                                   repoSlug: String,
                                                   pullRequestId: Long): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/pullrequests/$pullRequestId/comments"))
            parameters.append("fields", Fields.Comment.paged().queryParam())
        }.build()

internal fun BitbucketCloud.pullRequestCommentUrl(workspaceId: String,
                                                  repoSlug: String,
                                                  pullRequestId: Long,
                                                  id: Long): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/pullrequests/$pullRequestId/comments/$id"))
            parameters.append("fields", Fields.Comment.queryParam())
        }.build()

internal data class CommentBean(val id: Long,
                                val content: CommentContentBean,
                                @JsonProperty("created_on") val createdDate: Instant,
                                @JsonProperty("updated_on") val updatedDate: Instant,
                                val user: UserBean?,
                                val inline: CommentAnchorBean?) {
    fun toComment() =
            Comment(
                    id = id,
                    content = content.toCommentContent(),
                    createdDate = createdDate,
                    updatedDate = updatedDate,
                    author = user?.toUser(),
                    anchor = inline?.toCommentAnchor()
            )
}

internal data class CommentContentBean(val html: String,
                                       val markup: String,
                                       val raw: String) {
    fun toCommentContent() = CommentContent(
            html = html,
            markup = markup,
            raw = raw
    )
}

internal data class CommentAnchorBean(private val to: Int?,
                                      private val from: Int?,
                                      val path: String) {
    fun toCommentAnchor() = CommentAnchor(
            filePath = path,
            sourceLineNumber = from,
            destinationLineNumber = to,
            orphaned = false
    )
}
