package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.common.*
import com.atlassian.bitbucket.cloud.rest.user.UserBean
import com.atlassian.bitbucket.util.resolve
import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom
import java.time.Instant

internal fun BitbucketCloud.pullRequestsUrl(workspaceId: String,
                                            repoSlug: String,
                                            bbqlQuery: String? = null): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/pullrequests"))
            bbqlQuery?.let { parameters.append("q", bbqlQuery) }
            parameters.append("fields", Fields.PullRequest.paged().queryParam())
        }.build()

internal fun BitbucketCloud.pullRequestUrl(workspaceId: String,
                                           repoSlug: String,
                                           id: Long): Url =
        URLBuilder().apply {
            takeFrom(apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/pullrequests/$id"))
            parameters.append("fields", Fields.PullRequest.queryParam())
        }.build()

internal fun BitbucketCloud.approvePullRequestUrl(workspaceId: String,
                                                  repoSlug: String,
                                                  id: Long): Url =
        apiBaseUrl.resolve("2.0/repositories/$workspaceId/$repoSlug/pullrequests/$id/approve")


internal data class PullRequestBean(val id: Long,
                                    val title: String,
                                    val description: String?,
                                    val state: PullRequestState,
                                    @JsonProperty("created_on") val createdDate: Instant,
                                    @JsonProperty("updated_on") val updatedDate: Instant,
                                    val destination: CommitCoordinatesBean,
                                    val source: CommitCoordinatesBean,
                                    val author: UserBean?,
                                    val reviewers: List<UserBean> = listOf(),
                                    val links: LinksBean) {
    fun toPullRequest() = PullRequest(
            id = id,
            title = title,
            description = description,
            state = state,
            createdDate = createdDate,
            updatedDate = updatedDate,
            destination = destination.toCommitCoordinates(),
            source = source.toCommitCoordinates(),
            author = author?.toUser(),
            reviewers = reviewers.map { it.toUser() },
            links = links.toLinks()
    )
}

internal data class CommitCoordinatesBean(val branch: BranchBean,
                                          val commit: CommitBean?,
                                          val repository: RepositoryInfoBean?) {
    fun toCommitCoordinates() = CommitCoordinates(
            branchName = branch.name,
            commitHash = commit?.hash,
            repositoryInfo = repository?.toRepositoryInfo()
    )
}

internal data class BranchBean(val name: String)

internal data class CommitBean(val hash: String)

internal data class RepositoryInfoBean(val name: String,
                                       val fullName: String) {
    fun toRepositoryInfo() = RepositoryInfo(
            name = name,
            workspaceId = fullName.substringBefore('/'),
            slug = fullName.substringAfter('/')
    )
}
