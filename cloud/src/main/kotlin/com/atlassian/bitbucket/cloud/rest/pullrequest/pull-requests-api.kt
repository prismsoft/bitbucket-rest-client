package com.atlassian.bitbucket.cloud.rest.pullrequest

import com.atlassian.bitbucket.cloud.rest.CommitHash
import com.atlassian.bitbucket.cloud.rest.common.Links
import com.atlassian.bitbucket.cloud.rest.pagination.CloudPage
import com.atlassian.bitbucket.cloud.rest.user.User
import java.time.Instant

interface PullRequestApi {
    fun comment(commentId: Long): PullRequestCommentApi
    fun comments(): PullRequestCommentsApi

    suspend fun get(): PullRequest
    suspend fun approve()
    suspend fun unapprove()
}

interface PullRequestsApi {
    fun withAuthor(nickname: String): PullRequestsApi
    fun withReviewer(nickname: String): PullRequestsApi
    fun withState(state: PullRequestState): PullRequestsApi
    fun withTargetBranch(branchName: String): PullRequestsApi

    suspend fun page(): CloudPage<PullRequest>
}

data class PullRequest(val id: Long,
                       val title: String,
                       val description: String?,
                       val state: PullRequestState,
                       val createdDate: Instant,
                       val updatedDate: Instant,
                       val destination: CommitCoordinates,
                       val source: CommitCoordinates,
                       val author: User?,
                       val reviewers: List<User>,
                       val links: Links)

data class CommitCoordinates(val branchName: String,
                             val commitHash: CommitHash?,
                             val repositoryInfo: RepositoryInfo?)

data class RepositoryInfo(val name: String,
                          val workspaceId: String,
                          val slug: String) {
    val fullSlug = "$workspaceId/$slug"
}

enum class PullRequestState {
    MERGED, SUPERSEDED, OPEN, DECLINED
}
