package com.atlassian.bitbucket.cloud.rest

import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.matching.UrlPathPattern
import com.github.tomakehurst.wiremock.matching.UrlPattern
import io.ktor.http.Url

internal fun testInstance(baseUri: String) = BitbucketCloud.CustomInstance(Url(baseUri))

internal fun apiUrlEqualTo(testUrl: String) = UrlPattern(equalTo("/!api$testUrl"), false)

internal fun apiUrlPathEqualTo(path: String) = UrlPathPattern(equalTo("/!api$path"), false)
