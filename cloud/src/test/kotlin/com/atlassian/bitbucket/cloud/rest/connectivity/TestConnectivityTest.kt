package com.atlassian.bitbucket.cloud.rest.connectivity

import com.atlassian.bitbucket.cloud.rest.*
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock.*
import io.ktor.http.HttpHeaders
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class TestConnectivityTest {

    @Test
    fun `good hook_events response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("hook-events-response.json")
        server.stubFor(get(apiUrlEqualTo("/2.0/hook_events"))
                .willReturn(okJson(json)))

        BitbucketCloudApi { instance = testInstance(uri) }
                .use { api ->
                    runBlocking {
                        api.testConnectivity().unauthenticatedResource()
                    }
                }
    }

    @Test
    fun `unrecognized hook_events response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("unrecognized-response.json")
        server.stubFor(get(apiUrlEqualTo("/2.0/hook_events"))
                .willReturn(okJson(json)))

        BitbucketCloudApi { instance = testInstance(uri) }
                .use { api ->
                    assertThrows<MalformedResponse> {
                        runBlocking {
                            api.testConnectivity().unauthenticatedResource()
                        }
                    }
                }
    }

    @Test
    fun `good user response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("user-response.json")
        server.stubFor(get(apiUrlEqualTo("/2.0/user"))
                .willReturn(bitbucketUnauthorized()))
        server.stubFor(get(apiUrlEqualTo("/2.0/user"))
                .withBasicAuth("user", "password")
                .willReturn(okJson(json)))

        BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }.use { api ->
            runBlocking {
                api.testConnectivity().authenticatedResource()
            }
        }
    }

    @Test
    fun `unrecognized user response`(@Wiremock server: WireMockServer, @WiremockUri uri: String) {
        val json = readResourceAsString("unrecognized-response.json")
        server.stubFor(get(apiUrlEqualTo("/2.0/user"))
                .willReturn(bitbucketUnauthorized()))
        server.stubFor(get(apiUrlEqualTo("/2.0/user"))
                .withBasicAuth("user", "password")
                .willReturn(okJson(json)))

        BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }.use { api ->
            assertThrows<MalformedResponse> {
                runBlocking {
                    api.testConnectivity().authenticatedResource()
                }
            }
        }
    }

    private fun bitbucketUnauthorized(): ResponseDefinitionBuilder =
            unauthorized().withHeader(HttpHeaders.WWWAuthenticate, "Basic realm=\"example.com HTTP\"")
}
