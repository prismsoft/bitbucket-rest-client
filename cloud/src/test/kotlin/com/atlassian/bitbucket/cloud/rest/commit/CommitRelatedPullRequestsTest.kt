package com.atlassian.bitbucket.cloud.rest.commit

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.atlassian.bitbucket.cloud.rest.*
import com.atlassian.bitbucket.cloud.rest.common.Fields
import com.atlassian.bitbucket.cloud.rest.common.Link
import com.atlassian.bitbucket.cloud.rest.common.paged
import com.atlassian.bitbucket.cloud.rest.pullrequest.CommitCoordinates
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequest
import com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequestState
import com.atlassian.bitbucket.cloud.rest.pullrequest.RepositoryInfo
import com.atlassian.bitbucket.cloud.rest.user.User
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri
import java.net.URI
import java.time.Instant

private const val TITLE = "Send to more than one email, and fix email sending in py3"
private const val DESCRIPTION = "* Send email to one or more addresses..."
private val CREATED = Instant.parse("2017-06-14T13:56:48.628105Z")
private val UPDATED = Instant.parse("2017-06-22T20:55:15.053520Z")

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class CommitRelatedPullRequestsTest {

    private lateinit var api: BitbucketCloudApi
    private lateinit var server: WireMockServer

    @BeforeEach
    internal fun setUp(@Wiremock wiremock: WireMockServer, @WiremockUri uri: String) {
        api = BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }
        server = wiremock
    }

    @AfterEach
    internal fun tearDown() = api.close()

    @Test
    fun `get related pull requests for a commit`() {
        val json = readResourceAsString("related-pull-requests.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/commit/0e773ff/pullrequests"))
                .withFieldsQueryParameter(Fields.PullRequest.paged())
                .willReturn(okJson(json)))

        val prPage = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .commit("0e773ff")
                    .pagePullRequests()
        }

        assertThat(prPage.totalSize).isEqualTo(1)
        assertThat(prPage.pageLength).isEqualTo(25)

        val pullRequests = prPage.items
        assertThat(pullRequests, "pull requests number").hasSize(1)
        assertThat(pullRequests.single(), "pull request").isEqualTo(
                PullRequest(
                        id = 6,
                        title = TITLE,
                        description = DESCRIPTION,
                        state = PullRequestState.MERGED,
                        createdDate = CREATED,
                        updatedDate = UPDATED,
                        destination = CommitCoordinates(
                                branchName = "master",
                                commitHash = "3928288dc2e9",
                                repositoryInfo = RepositoryInfo(
                                        name = "dogslow",
                                        workspaceId = "evzijst",
                                        slug = "dogslow"
                                )
                        ),
                        source = CommitCoordinates(
                                branchName = "more-than-one-email-fix-py3",
                                commitHash = "a8bc542fac9d",
                                repositoryInfo = RepositoryInfo(
                                        name = "dogslow",
                                        workspaceId = "tgs",
                                        slug = "dogslow"
                                )
                        ),
                        author = User(
                                name = "Thomas Smith",
                                nickname = "tgs",
                                accountId = "tgs-account-id",
                                links = mapOf("avatar" to Link(URI.create("https://bitbucket.org/account/tgs/avatar/32/")))
                        ),
                        reviewers = listOf(
                                User(
                                        name = "Erik van Zijst",
                                        nickname = "evzijst",
                                        accountId = "evzijst-account-id",
                                        links = mapOf("avatar" to Link(URI.create("https://bitbucket.org/account/evzijst/avatar/32/")))
                                )
                        ),
                        links = mapOf("html" to Link(URI.create("https://bitbucket.org/evzijst/dogslow/pull-requests/6")))
                )
        )
    }

    @Test
    fun `repository pull requests are indexing`() {
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/commit/0e773ff/pullrequests"))
                .withFieldsQueryParameter(Fields.PullRequest.paged())
                .willReturn(aResponse().withStatus(202)))

        assertThrows<PullRequestLinksIndexingException> {
            runBlocking {
                api.repository("aWorkspaceId", "aSlug")
                        .commit("0e773ff")
                        .pagePullRequests()
            }
        }
    }

    @ParameterizedTest
    @ValueSource(ints = [400, 404])
    fun `pull request links add-on is not installed`(statusCode: Int) {
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/commit/0e773ff/pullrequests"))
                .withFieldsQueryParameter(Fields.PullRequest.paged())
                .willReturn(aResponse().withStatus(statusCode)))

        assertThrows<PullRequestLinksNotEnabledException> {
            runBlocking {
                api.repository("aWorkspaceId", "aSlug")
                        .commit("0e773ff")
                        .pagePullRequests()
            }
        }
    }
}
