package com.atlassian.bitbucket.cloud.rest.pullrequest

import assertk.assertThat
import assertk.assertions.*
import com.atlassian.bitbucket.cloud.rest.*
import com.atlassian.bitbucket.cloud.rest.common.Fields
import com.atlassian.bitbucket.cloud.rest.common.paged
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri
import java.time.Instant

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class PullRequestsTest {

    private lateinit var api: BitbucketCloudApi
    private lateinit var server: WireMockServer

    @BeforeEach
    internal fun setUp(@Wiremock wiremock: WireMockServer, @WiremockUri uri: String) {
        api = BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }
        server = wiremock
    }

    @AfterEach
    internal fun tearDown() = api.close()

    @Test
    fun `get pull request by id`() {
        val json = readResourceAsString("pull-request.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/6"))
                .withFieldsQueryParameter(Fields.PullRequest)
                .willReturn(okJson(json)))

        val pullRequest = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(6)
                    .get()
        }

        assertPullRequest(pullRequest)
    }

    @Test
    fun `pull request lookup`() {
        val json = readResourceAsString("pull-requests.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests"))
                .withBbqlQueryParameter("author.nickname=\"author\" AND reviewers.nickname=\"reviewer\" " +
                        "AND state=\"MERGED\" AND destination.branch.name=\"master\"")
                .withFieldsQueryParameter(Fields.PullRequest.paged())
                .willReturn(okJson(json)))

        val page = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequests()
                    .withAuthor("author")
                    .withReviewer("reviewer")
                    .withState(PullRequestState.MERGED)
                    .withTargetBranch("master")
                    .page()
        }

        assertThat(page.number, "page number").isEqualTo(1)
        assertThat(page.pageLength, "page length").isEqualTo(25)
        assertThat(page.totalSize, "total number of pull requests").isEqualTo(1)
        assertThat(page.items, "number of pull requests on the page").hasSize(1)
        assertPullRequest(page.items.single())
    }

    @Test
    fun `pull request with no author`() {
        val json = readResourceAsString("pull-request-empty-author.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/1"))
                .withQueryParam("q", absent())
                .withFieldsQueryParameter(Fields.PullRequest)
                .willReturn(okJson(json)))

        val pullRequest = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(1)
                    .get()
        }

        assertThat(pullRequest, "pull request").isNotNull()
        assertThat(pullRequest.author, "pull request author").isNull()
    }

    @Test
    fun `approve pull request`() {
        server.stubFor(any(anyUrl()).willReturn(ok()))

        runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(5)
                    .approve()
        }

        server.verify(postRequestedFor(apiUrlEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/5/approve")))
    }

    @Test
    fun `unapprove pull request`() {
        server.stubFor(any(anyUrl()).willReturn(ok()))

        runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(10)
                    .unapprove()
        }

        server.verify(deleteRequestedFor(apiUrlEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/10/approve")))
    }

    private fun assertPullRequest(pullRequest: PullRequest) {
        assertThat(pullRequest.id, "pull request ID").isEqualTo(6L)
        assertThat(pullRequest.title, "pull request title").isEqualTo("Added Create Pull Request action")
        assertThat(pullRequest.description, "pull request description").isEqualTo("Create pull request in Bitbucket feature was added")
        assertThat(pullRequest.state, "pull request state").isEqualTo(PullRequestState.MERGED)
        assertThat(pullRequest.createdDate, "pull request create date").isEqualTo(Instant.parse("2016-05-25T23:02:32.203282000Z"))
        assertThat(pullRequest.updatedDate, "pull request update date").isEqualTo(Instant.parse("2016-05-26T12:01:45.401703000Z"))

        assertThat(pullRequest.destination.branchName, "pull request destination branch name").isEqualTo("master")
        assertThat(pullRequest.destination.commitHash, "pull request destination branch commit hash").isEqualTo("c27fce9f7d5a")
        assertThat(pullRequest.destination.repositoryInfo?.workspaceId, "pull request destination repo workspace").isEqualTo("atlassianlabs")
        assertThat(pullRequest.destination.repositoryInfo?.name, "pull request destination repo name").isEqualTo("intellij-bitbucket-references-plugin")
        assertThat(pullRequest.destination.repositoryInfo?.slug, "pull request destination repo slug").isEqualTo("intellij-bitbucket-references-plugin")
        assertThat(pullRequest.destination.repositoryInfo?.fullSlug, "pull request destination repo full slug").isEqualTo("atlassianlabs/intellij-bitbucket-references-plugin")

        assertThat(pullRequest.source.branchName, "pull request source branch name").isEqualTo("pwilczynski/feature/pull-requests")
        assertThat(pullRequest.source.commitHash, "pull request source branch commit hash").isEqualTo("fd18a8474093")
        assertThat(pullRequest.source.repositoryInfo?.workspaceId, "pull request source repo workspace").isEqualTo("delwing")
        assertThat(pullRequest.source.repositoryInfo?.name, "pull request source repo name").isEqualTo("intellij-bitbucket-references-plugin")
        assertThat(pullRequest.source.repositoryInfo?.slug, "pull request source repo slug").isEqualTo("intellij-bitbucket-references-plugin")
        assertThat(pullRequest.source.repositoryInfo?.fullSlug, "pull request source repo full slug").isEqualTo("delwing/intellij-bitbucket-references-plugin")

        assertThat(pullRequest.author?.name, "pull request author display name").isEqualTo("Piotr W")
        assertThat(pullRequest.author?.nickname, "pull request author nickname").isEqualTo("delwing")

        assertThat(pullRequest.reviewers, "pull request reviewers number").hasSize(1)
        val reviewer = pullRequest.reviewers.single()
        assertThat(reviewer.name, "pull request reviewer display name").isEqualTo("Daniil Penkin")
        assertThat(reviewer.nickname, "pull request reviewer nickname").isEqualTo("dpenkin")

        val links = pullRequest.links
        assertThat(links, "pull requests links number").hasSize(10)
        assertThat(links["html"]?.href, "pull request HTML link").hasToString("https://bitbucket.org/atlassianlabs/intellij-bitbucket-references-plugin/pull-requests/6")
    }
}
