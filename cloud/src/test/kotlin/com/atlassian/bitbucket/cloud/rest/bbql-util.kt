package com.atlassian.bitbucket.cloud.rest

import com.atlassian.bitbucket.cloud.rest.common.FieldList
import com.atlassian.bitbucket.cloud.rest.common.queryParam
import com.github.tomakehurst.wiremock.client.MappingBuilder
import com.github.tomakehurst.wiremock.client.WireMock.equalTo

internal fun MappingBuilder.withBbqlQueryParameter(query: String): MappingBuilder =
        withQueryParam("q", equalTo(query))

internal fun MappingBuilder.withFieldsQueryParameter(fields: FieldList): MappingBuilder =
        withQueryParam("fields", equalTo(fields.queryParam()))
