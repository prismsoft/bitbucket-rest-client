package com.atlassian.bitbucket.cloud.rest.pullrequest

import assertk.assertThat
import assertk.assertions.*
import com.atlassian.bitbucket.cloud.rest.*
import com.atlassian.bitbucket.cloud.rest.common.Fields
import com.atlassian.bitbucket.cloud.rest.common.paged
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.okJson
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri
import java.time.Instant

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class PullRequestCommentsTest {

    private lateinit var api: BitbucketCloudApi
    private lateinit var server: WireMockServer

    @BeforeEach
    internal fun setUp(@Wiremock wiremock: WireMockServer, @WiremockUri uri: String) {
        api = BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }
        server = wiremock
    }

    @AfterEach
    internal fun tearDown() = api.close()

    @Test
    fun `get pull request comment by id`() {
        val json = readResourceAsString("pull-request-comment.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/1/comments/24907921"))
                .withFieldsQueryParameter(Fields.Comment)
                .willReturn(okJson(json)))

        val comment = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(1)
                    .comment(24907921)
                    .get()
        }

        assertInlineComment(comment)
    }

    @Test
    fun `pull request comment lookup`() {
        val json = readResourceAsString("pull-request-comments.json")
        server.stubFor(get(apiUrlPathEqualTo("/2.0/repositories/aWorkspaceId/aSlug/pullrequests/7/comments"))
                .withFieldsQueryParameter(Fields.Comment.paged())
                .willReturn(okJson(json)))

        val page = runBlocking {
            api.repository("aWorkspaceId", "aSlug")
                    .pullRequest(7)
                    .comments()
                    .page()
        }

        assertThat(page.number, "page number").isEqualTo(3)
        assertThat(page.pageLength, "page length").isEqualTo(2)
        assertThat(page.totalSize, "total number of pull requests").isEqualTo(5)
        assertThat(page.items, "number of pull requests on the page").hasSize(1)
        assertInlineComment(page.items.single())
    }

    private fun assertInlineComment(comment: Comment) {
        assertThat(comment.id, "comment ID").isEqualTo(24907921L)
        assertThat(comment.content.html, "comment HTML content").isEqualTo("<p>This gonna be outdated comment</p>")
        assertThat(comment.content.raw, "comment raw content").isEqualTo("This gonna be outdated comment")
        assertThat(comment.content.markup, "comment markup type").isEqualTo("markdown")
        assertThat(comment.createdDate, "comment create date")
                .isEqualTo(Instant.parse("2016-10-09T09:59:23.925223Z"))
        assertThat(comment.updatedDate, "comment update date")
                .isEqualTo(Instant.parse("2016-10-09T09:59:23.929745Z"))
        assertThat(comment.author?.name, "comment author display name").isEqualTo("Daniil Penkin")
        assertThat(comment.author?.nickname, "comment author nickname").isEqualTo("dpenkin")

        val anchor = comment.anchor
        assertThat(anchor, "comment anchor").isNotNull().let {
            it.given { actual ->
                assertThat(actual.filePath, "comment anchor file path").isEqualTo("lorem.txt")
                assertThat(actual.sourceLineNumber, "comment anchor source line number").isNull()
                assertThat(actual.destinationLineNumber, "comment anchor destination line number").isEqualTo(4)
                assertThat(actual.orphaned, "comment anchor orphaned flag").isFalse()
            }
        }
    }
}
