package com.atlassian.bitbucket.cloud.rest.snippet

import assertk.assertThat
import assertk.assertions.*
import com.atlassian.bitbucket.cloud.rest.*
import com.atlassian.bitbucket.test.readResourceAsString
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri
import java.io.InputStream

@ExtendWith(WiremockResolver::class, WiremockUriResolver::class)
class CreateSnippetTest {

    private lateinit var api: BitbucketCloudApi
    private lateinit var server: WireMockServer

    @BeforeEach
    internal fun setUp(@Wiremock wiremock: WireMockServer, @WiremockUri uri: String) {
        api = BitbucketCloudApi {
            authentication = Authentication.Basic("user", "password")
            instance = testInstance(uri)
        }
        server = wiremock
    }

    @AfterEach
    internal fun tearDown() = api.close()

    @Test
    fun `create user snippet`() {
        val json = readResourceAsString("user-snippet.json")
        server.stubFor(post(apiUrlPathEqualTo("/2.0/snippets"))
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("title")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=title"))
                                .withBody(equalTo("Hooray"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("is_private")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=is_private"))
                                .withBody(equalTo("true"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("scm")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=scm"))
                                .withBody(equalTo("git"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("file")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=file"))
                                .withHeader("Content-Disposition", containing("filename=\"test.txt\""))
                                .withBody(equalTo("Hello world"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("file")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=file"))
                                .withHeader("Content-Disposition", containing("filename=\"another.png\""))
                                .withBody(equalTo("Foo Bar"))
                )
                .willReturn(okJson(json)))

        val snippet = runBlocking {
            api.snippets()
                    .create()
                    .withTitle("Hooray")
                    .withAccess(Access.PRIVATE)
                    .withScm(Scm.GIT)
                    .withFile("test.txt") { "Hello world".byteInputStream() }
                    .withFile("another.png") { "Foo Bar".byteInputStream() }
                    .save()
        }

        assertThat(snippet.id).isEqualTo("XeXzbp")

        val files = snippet.files
        assertThat(files).hasSize(2)
        assertThat(files).key("test.txt").hasSize(2)
        assertThat(files).key("another.png").hasSize(2)

        assertThat(snippet.links["html"]?.href).hasToString("https://bitbucket.org/snippets/dpenkin/XeXzbp")
        assertThat(snippet.links["clone.ssh"]?.href).hasToString("ssh://git@bitbucket.org:snippets/dpenkin/XeXzbp/hooray.git")
    }

    @Test
    fun `create team snippet`() {
        val json = readResourceAsString("team-snippet.json")
        server.stubFor(post(apiUrlPathEqualTo("/2.0/snippets/bitbucket"))
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("title")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=title"))
                                .withBody(equalTo("Hooray"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("is_private")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=is_private"))
                                .withBody(equalTo("true"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("scm")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=scm"))
                                .withBody(equalTo("git"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("file")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=file"))
                                .withHeader("Content-Disposition", containing("filename=\"test.txt\""))
                                .withBody(equalTo("Hello world"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                // TODO replace with `.withName("file")` when WireMock allows quote-less values
                                .withHeader("Content-Disposition", containing("name=file"))
                                .withHeader("Content-Disposition", containing("filename=\"another.png\""))
                                .withBody(equalTo("Foo Bar"))
                )
                .willReturn(okJson(json)))

        val snippet = runBlocking {
            api.snippets()
                    .create("bitbucket")
                    .withTitle("Hooray")
                    .withAccess(Access.PRIVATE)
                    .withScm(Scm.GIT)
                    .withFile("test.txt") { "Hello world".byteInputStream() }
                    .withFile("another.png") { "Foo Bar".byteInputStream() }
                    .save()
        }

        assertThat(snippet.id).isEqualTo("XeXzbp")

        val files = snippet.files
        assertThat(files).hasSize(2)
        assertThat(files).key("test.txt").hasSize(2)
        assertThat(files).key("another.png").hasSize(2)

        assertThat(snippet.links["html"]?.href).hasToString("https://bitbucket.org/snippets/dpenkin/XeXzbp")
        assertThat(snippet.links["clone.ssh"]?.href).hasToString("ssh://git@bitbucket.org:snippets/dpenkin/XeXzbp/hooray.git")
    }

    @Test
    fun `input streams closed`() {
        server.stubFor(post(apiUrlPathEqualTo("/2.0/snippets"))
                .willReturn(aResponse().withStatus(400)))

        val spies = mutableListOf<InputStream>()

        assertThrows<BitbucketCloudRestClientException> {
            runBlocking {
                api.snippets()
                        .create()
                        .withFile("test.txt") { spyk("Hello".byteInputStream()).also { spies.add(it) } }
                        .withFile("another.png") { spyk("World".byteInputStream()).also { spies.add(it) } }
                        .save()
            }
        }

        assertThat(spies).size().isGreaterThan(0)
        spies.forEach {
            verify { it.close() }
        }
    }
}
