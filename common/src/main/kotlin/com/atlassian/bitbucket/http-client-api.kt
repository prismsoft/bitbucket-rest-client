package com.atlassian.bitbucket

import io.ktor.client.HttpClient

enum class HttpClientLifecyclePolicy {
    /**
     * HTTP client lifecycle is bound to Bitbucket REST API client,
     * i.e. HTTP client will be [closed][HttpClient.close] together with the latter.
     */
    BOUND,

    /**
     * HTTP client lifecycle is unrelated to Bitbucket REST API client lifecycle,
     * i.e. it is controlled externally.
     */
    UNRELATED
}
