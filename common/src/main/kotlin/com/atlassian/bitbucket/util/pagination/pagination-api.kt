package com.atlassian.bitbucket.util.pagination

import java.net.URI

interface Page<T, A : PaginationApiProvider> {
    val pageLength: Int
    val items: List<T>
    val nextPageUrl: URI?
    val paginator: (A) -> PaginationApi<Page<T, A>, A, T>?
}

/**
 * Marker interface to be used for [Page.paginator].
 */
interface PaginationApiProvider

interface PaginationApi<out P : Page<T, A>, A : PaginationApiProvider, T> {

    suspend fun nextPage(): P
}
