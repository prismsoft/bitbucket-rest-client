package com.atlassian.bitbucket.util

import com.atlassian.bitbucket.HttpClientLifecyclePolicy
import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.http.Parameters
import io.ktor.http.Url
import io.ktor.http.content.PartData
import io.ktor.http.takeFrom

suspend inline fun <reified T> HttpClient.submitForm(
        url: Url,
        formData: Parameters = Parameters.Empty,
        encodeInQuery: Boolean = false,
        block: HttpRequestBuilder.() -> Unit = {}
): T = submitForm(formData, encodeInQuery) {
    this.url.takeFrom(url)
    block()
}

suspend inline fun <reified T> HttpClient.submitFormWithBinaryData(
        url: Url,
        formData: List<PartData> = emptyList(),
        block: HttpRequestBuilder.() -> Unit = {}
): T = submitFormWithBinaryData(formData) {
    this.url.takeFrom(url)
    block()
}

fun HttpClient.closeIfRequired(lifecyclePolicy: HttpClientLifecyclePolicy) {
    if (lifecyclePolicy == HttpClientLifecyclePolicy.BOUND) {
        close()
    }
}
