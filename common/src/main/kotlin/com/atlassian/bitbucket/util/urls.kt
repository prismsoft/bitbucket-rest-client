package com.atlassian.bitbucket.util

import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.takeFrom

// TODO test this
fun Url.resolve(path: String): Url =
        URLBuilder().apply {
            takeFrom(this@resolve)
            encodedPath = if (path.startsWith("/")) {
                path
            } else {
                "${encodedPath.removeSuffix("/")}/$path"
            }
        }.build()
